#include "src/polyfill/windows/winver.hpp"

#include <Windows.h>

typedef void(WINAPI *foo_t)( __out_opt ULONG*, __out_opt ULONG*, __out_opt ULONG* );

static bool getIsWindows11() noexcept {
	HMODULE dll = GetModuleHandleA( "ntdll.dll" );
	if( !dll ) return false;
	
	foo_t RtlGetNtVersionNumbers = (foo_t)GetProcAddress( dll, "RtlGetNtVersionNumbers" );
	if( !RtlGetNtVersionNumbers ) return false;

	ULONG major = 0;
	ULONG minor = 0;
	ULONG build = 0;
	
	RtlGetNtVersionNumbers( &major, &minor, &build );
	
	if( major == 11u ) return true;
	if( major != 10u ) return false;
	if( minor != 0u ) return true;
	return (build & 0x0FFFFFFFu) >= 22000u;
}

bool WindowsVersion::isWindows11() noexcept {
	static const bool isWin11 = getIsWindows11();
	return isWin11;
}
