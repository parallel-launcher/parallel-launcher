#ifndef SRC_POLYFILL_WINDOWS_WINVER_HPP_
#define SRC_POLYFILL_WINDOWS_WINVER_HPP_

namespace WindowsVersion {

	extern bool isWindows11() noexcept;

}

#endif /* SRC_POLYFILL_WINDOWS_WINVER_HPP_ */
