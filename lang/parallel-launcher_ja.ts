<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja" sourcelanguage="en_CA">
<context>
    <name>AbstractCoreFinderDialog</name>
    <message>
        <location filename="../src/ui/core-finder-dialog.cpp" line="10"/>
        <source>Searching for latest core version...</source>
        <translation>最新のコアバージョンを検索中...</translation>
    </message>
</context>
<context>
    <name>BindInputDialog</name>
    <message>
        <location filename="../src/ui/designer/bind-input-dialog.ui" line="14"/>
        <source>Bind Input</source>
        <translation>入力割り当て</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/bind-input-dialog.ui" line="38"/>
        <source>To map a button or axis to this input, either press a button on your controller or hold a control stick or trigger in a different position for a half second.</source>
        <translation>ボタンや軸をこの入力にマッピングするには、コントローラーのボタンを押すか、コントロールスティックやトリガーを別の位置で半秒間保持します。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/bind-input-dialog.ui" line="76"/>
        <source>Skip</source>
        <translation>スキップ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/bind-input-dialog.ui" line="87"/>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
</context>
<context>
    <name>ControllerConfigDialog</name>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="14"/>
        <source>Configure Controller</source>
        <translation>コントローラーの設定</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="25"/>
        <source>Configuring Controller:</source>
        <translation>コントローラーの設定:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="62"/>
        <source>Quick Configure</source>
        <translation>クイック設定</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="86"/>
        <source>Enable Rumble</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="99"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="19"/>
        <source>Analog Up</source>
        <translation>スティック&#x3000;上</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="122"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="20"/>
        <source>Analog Down</source>
        <translation>スティック&#x3000;下</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="145"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="21"/>
        <source>Analog Left</source>
        <translation>スティック&#x3000;左</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="168"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="22"/>
        <source>Analog Right</source>
        <translation>スティック&#x3000;右</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="191"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="31"/>
        <source>A Button</source>
        <translation>Aボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="214"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="32"/>
        <source>B Button</source>
        <translation>Bボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="248"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="23"/>
        <source>C Up</source>
        <translation>C&#x3000;上</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="271"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="24"/>
        <source>C Down</source>
        <translation>C&#x3000;下</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="294"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="25"/>
        <source>C Left</source>
        <translation>C&#x3000;左</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="317"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="26"/>
        <source>C Right</source>
        <translation>C&#x3000;右</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="340"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="34"/>
        <source>Z Trigger</source>
        <translation>Zボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="363"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="35"/>
        <source>R Trigger</source>
        <translation>Rボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="397"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="27"/>
        <source>D-Pad Up</source>
        <translation>Dボタン&#x3000;上</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="420"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="28"/>
        <source>D-Pad Down</source>
        <translation>Dボタン&#x3000;下</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="443"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="29"/>
        <source>D-Pad Left</source>
        <translation>Dボタン&#x3000;左</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="466"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="30"/>
        <source>D-Pad Right</source>
        <translation>Dボタン&#x3000;右</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="489"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="36"/>
        <source>Start Button</source>
        <translation>スタートボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="512"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="33"/>
        <source>L Trigger</source>
        <translation>Lボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="546"/>
        <source>The following inputs are only used by games that support Gamecube Controllers:</source>
        <translation>以下の入力は、ゲームキューブコントローラをサポートするゲームでのみ使用されます:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="566"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="37"/>
        <source>X Button</source>
        <translation>Xボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="573"/>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="597"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="75"/>
        <source>Not Bound</source>
        <translation>未割り当て</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="610"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="38"/>
        <source>Y Button</source>
        <translation>Yボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="660"/>
        <source>Sensitivity</source>
        <translation>感度</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="686"/>
        <source>Deadzone</source>
        <translation>デッドゾーン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="745"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="39"/>
        <source>Save State</source>
        <translation>状態をセーブ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="774"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="40"/>
        <source>Load State</source>
        <translation>状態をロード</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="822"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="42"/>
        <source>Toggle Slow Motion</source>
        <translation>スローモーションの切り替え</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="832"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="41"/>
        <source>Toggle Fast Forward</source>
        <translation>早送りの切り替え</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="864"/>
        <source>How far you need to press down a trigger or tilt a control stick that is bound to an N64 button before it is considered a button press</source>
        <translation>N64のボタンに割り当てられたボタンを押したり、コントロールスティックを傾けたりする際に、どの程度までならボタンを押したとみなされるか</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="867"/>
        <source>Analog to Digital Button Press Threshold</source>
        <translation>アナログからデジタルへのボタン押下のしきい値</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="949"/>
        <source>Save As</source>
        <translation>名前を付けて保存</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="969"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="983"/>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="78"/>
        <source>Button %1</source>
        <translation>ボタン %1</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="81"/>
        <source>Axis -%1</source>
        <translation>軸 -%1</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="84"/>
        <source>Axis +%1</source>
        <translation>軸 +%1</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="87"/>
        <source>Hat %1 Up</source>
        <translation>Dパッド %1 上</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="90"/>
        <source>Hat %1 Down</source>
        <translation>Dパッド %1 下</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="93"/>
        <source>Hat %1 Left</source>
        <translation>Dパッド %1 左</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="96"/>
        <source>Hat %1 Right</source>
        <translation>Dパッド %1 右</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="141"/>
        <source>Enter Profile Name</source>
        <translation>プロファイル名を入力</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="141"/>
        <source>Enter a name for your new controller profile.</source>
        <translation>新しいコントローラープロファイル名を入力。</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="147"/>
        <source>Failed to Save Profile</source>
        <translation>プロファイルの保存に失敗しました</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="147"/>
        <source>A default controller profile with that name already exists.</source>
        <translation>この名前のデフォルトコントローラプロファイルは既に存在します。</translation>
    </message>
</context>
<context>
    <name>ControllerSelectDialog</name>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="14"/>
        <source>Select Controller</source>
        <translation>コントローラーを選択</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="28"/>
        <source>Input Driver</source>
        <translation>入力ドライバー</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="81"/>
        <source>If your controller does not appear in this list, or if you are having problems setting up your controller bindings, try switching to a different input driver.</source>
        <translation>お使いのコントローラーがこのリストに表示されない場合、またはコントローラー割り当ての設定に問題がある場合は、別の入力ドライバーに切り替えてみてください。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="125"/>
        <source>Connected Controllers</source>
        <translation>接続されたコントローラー</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="154"/>
        <source>Controller Profiles</source>
        <translation>コントローラープロファイル</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="169"/>
        <location filename="../src/ui/controller-select-dialog.cpp" line="178"/>
        <location filename="../src/ui/controller-select-dialog.cpp" line="220"/>
        <source>Edit Profile</source>
        <translation>プロファイルを編集</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="180"/>
        <source>Delete Profile</source>
        <translation>プロファイルの削除</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-select-dialog.cpp" line="28"/>
        <source>Could not get connected controllers because a file (&lt;tt&gt;parallel-launcher-sdl-relay.exe&lt;/tt&gt;) is missing from your Parallel Launcher installation. Please re-install Parallel Launcher to restore the missing file(s). If this problem persists, you may need to whitelist the &lt;a href=&quot;#&quot;&gt;Parallel Launcher installation directory&lt;/a&gt; in your anti-virus software.</source>
        <translation>ファイルが原因でコントローラに接続できませんでした。 (&lt;tt&gt;parallel-launcher-sdl-relay.exe&lt;/tt&gt;) のファイルが Parallel Launcher にありません。</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-select-dialog.cpp" line="35"/>
        <source>Input driver failed to initialize due to an unexpected error.</source>
        <translation>予期せぬエラーにより、入力ドライバの初期化に失敗しました。</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-select-dialog.cpp" line="174"/>
        <location filename="../src/ui/controller-select-dialog.cpp" line="216"/>
        <source>New Profile</source>
        <translation>新しいプロファイル</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-select-dialog.cpp" line="269"/>
        <source>Confirm Delete</source>
        <translation>削除の確認</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-select-dialog.cpp" line="269"/>
        <source>Are you sure you want to delete controller profile &apos;%1&apos;?</source>
        <translation>コントローラ・プロファイル &apos;%1&apos; を本当に削除しますか?</translation>
    </message>
</context>
<context>
    <name>CoreInstaller</name>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="26"/>
        <location filename="../src/ui/core-installer.cpp" line="77"/>
        <source>Download Failed</source>
        <translation>ダウンロードに失敗しました</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="27"/>
        <location filename="../src/ui/core-installer.cpp" line="78"/>
        <source>Failed to download emulator core</source>
        <translation>エミュレータコアのダウンロードに失敗しました</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="35"/>
        <location filename="../src/ui/core-installer.cpp" line="110"/>
        <source>Installation Failed</source>
        <translation>インストールに失敗しました</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="36"/>
        <location filename="../src/ui/core-installer.cpp" line="111"/>
        <source>Failed to install emulator core</source>
        <translation>エミュレーターコアのインストールに失敗しました</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="46"/>
        <location filename="../src/ui/core-installer.cpp" line="122"/>
        <source>Installation Successful</source>
        <translation>インストール成功</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="47"/>
        <location filename="../src/ui/core-installer.cpp" line="123"/>
        <source>Core installed successfully.</source>
        <translation>コアのインストール成功。</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="150"/>
        <source>Core Unavailable</source>
        <translation>コア使用不可</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="151"/>
        <source>Sorry! The Mupen64plus-Next emulator core is not available on MacOS. Try using ParallelN64 instead.</source>
        <translation>申し訳ありません。Mupen64plus-NextエミュレータコアはMacOSでは使用できません。代わりにParallelN64を使ってみてください。</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="159"/>
        <source>Install Emulator Core?</source>
        <translation>エミュレーターコアをインストールしますか？</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="160"/>
        <source>This emulator core is not installed or is out of date. Would you like to install it now?</source>
        <translation>このエミュレータのコアがインストールされていないか、また旧式です。
今すぐインストールしますか？</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="214"/>
        <location filename="../src/ui/core-installer.cpp" line="245"/>
        <source>Core Update Available</source>
        <translation>コアのアップデートが可能です</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="215"/>
        <location filename="../src/ui/core-installer.cpp" line="246"/>
        <source>An update is available for %1. Would you like to install it now?</source>
        <translation>%1. のアップデートが可能です。今すぐインストールしますか？</translation>
    </message>
</context>
<context>
    <name>CrashReportDialog</name>
    <message>
        <location filename="../src/ui/designer/crash-report-dialog.ui" line="14"/>
        <source>View Crash Reports</source>
        <translation>クラッシュの報告</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/crash-report-dialog.ui" line="26"/>
        <source>Only show crashes from the current application version</source>
        <translation>現在のアプリケーションのバージョンのクラッシュのみを表示</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/crash-report-dialog.ui" line="61"/>
        <source>Crash Report</source>
        <translation>クラッシュの詳細</translation>
    </message>
</context>
<context>
    <name>DeviceBusyDialog</name>
    <message>
        <location filename="../src/ui/designer/device-busy-dialog.ui" line="17"/>
        <source>Device Busy</source>
        <translation>デバイスが使用中です</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/device-busy-dialog.ui" line="26"/>
        <source>One or more SD cards are busy. Waiting for all file operations to complete...</source>
        <translation>1枚以上のSDカードが使用中です。すべてのファイル操作が完了するのを待っています...</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/device-busy-dialog.ui" line="58"/>
        <source>Force Unmount</source>
        <translation>強制マウント解除</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/device-busy-dialog.ui" line="69"/>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
</context>
<context>
    <name>DirectPlay</name>
    <message>
        <location filename="../src/ui/play.cpp" line="672"/>
        <location filename="../src/ui/play.cpp" line="679"/>
        <location filename="../src/ui/play.cpp" line="686"/>
        <location filename="../src/ui/play.cpp" line="693"/>
        <source>Patch Failed</source>
        <translation>パッチの適用に失敗しました</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="673"/>
        <source>Failed to patch ROM. The .bps patch appears to be invalid.</source>
        <translation>ROMのパッチに失敗しました。.bpsパッチが無効のようです。</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="680"/>
        <source>Failed to patch ROM. The base ROM does not match what the patch expects..</source>
        <translation>ROMのパッチに失敗しました。ベースROMがパッチが期待するものと一致しません。</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="687"/>
        <source>Failed to patch ROM. The base rom required to patch is not known to Parallel Launcher.</source>
        <translation>ROMのパッチに失敗しました。パッチに必要なベースROMをParallel Launcherが認識できません。</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="694"/>
        <source>Failed to patch ROM. An error occurred while writing the patched ROM to disk.</source>
        <translation>ROM のパッチに失敗した。パッチを当てたROMをディスクに書き込む際にエラーが発生した。</translation>
    </message>
</context>
<context>
    <name>DirectPlayWindow</name>
    <message>
        <location filename="../src/ui/designer/direct-play-window.ui" line="14"/>
        <source>ROM Settings</source>
        <translation>ロム設定</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/direct-play-window.ui" line="27"/>
        <source>This is your first time running this ROM. Please select any ROM specific settings. You can always change these settings later.</source>
        <translation>このROMを初めて実行します。 ROM固有の設定があれば選択してください。 これらの設定は後でいつでも変更できます。</translation>
    </message>
</context>
<context>
    <name>DownloadDialog</name>
    <message>
        <location filename="../src/core/sdcard.mount.windows.cpp" line="423"/>
        <source>Downloading SD Card Manager Extension</source>
        <translation>拡張SDカードマネージャーをダウンロードしています</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="22"/>
        <location filename="../src/ui/core-installer.cpp" line="73"/>
        <source>Downloading core...</source>
        <translation>コアをダウンロードしています...</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="335"/>
        <source>Downloading Discord plugin</source>
        <translation>ディスコードプラグインをダウンロードしています</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="213"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="112"/>
        <source>Downloading RetroArch</source>
        <translation>RetroArchをダウンロードしています</translation>
    </message>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="53"/>
        <source>Downloading installer...</source>
        <translation>インストーラーをダウンロードしています...</translation>
    </message>
</context>
<context>
    <name>EditableStarDisplayWidget</name>
    <message>
        <location filename="../src/rhdc/ui/star-display-widget.cpp" line="371"/>
        <source>Save slot is empty.</source>
        <translation>セーブスロットは空です。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/star-display-widget.cpp" line="375"/>
        <source>Create Save Slot</source>
        <translation>セーブスロットを作成</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/star-display-widget.cpp" line="376"/>
        <source>Delete Save Slot</source>
        <translation>セーブスロットを削除</translation>
    </message>
</context>
<context>
    <name>ErrorNotifier</name>
    <message>
        <location filename="../src/ui/error-notifier.cpp" line="5"/>
        <source>The application encountered an unexpected error:</source>
        <translation>アプリケーションが予期せぬエラーに遭遇しました:</translation>
    </message>
    <message>
        <location filename="../src/ui/error-notifier.cpp" line="12"/>
        <source>Unhandled Error</source>
        <translation>予期せぬエラー</translation>
    </message>
</context>
<context>
    <name>Game</name>
    <message>
        <location filename="../src/ui/play.cpp" line="42"/>
        <source>The emulator exited shortly after launching. If you are able to launch other ROMs without issues, then this ROM may contain invalid or unsafe RSP microcode that cannot be run on console accurate graphics plugins. Alternatively, if you have a very old onboard graphics card, it is possible that Vulkan is not supported on your system. In either case, using another graphics plugin might resolve the issue.</source>
        <translation>起動後すぐにエミュレータが終了しました。他のROMを問題なく起動できる場合、このROMにはコンソールの正確なグラフィックプラグインで実行できない無効または安全でないRSPマイクロコードが含まれている可能性があります。あるいは、非常に古いオンボードグラフィックスカードを使用している場合、Vulkanがシステムでサポートされていない可能性もあります。いずれの場合も、別のグラフィックスプラグインを使用することで問題が解決する可能性があります。</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="48"/>
        <source>The emulator exited shortly after launching. If you are able to launch other ROMs without issues, then this ROM may contain invalid or unsafe RSP microcode that cannot be run on console accurate graphics plugins. If this is the case, try running the ROM with another graphics plugin instead.</source>
        <translation>起動後すぐにエミュレータが終了しました。他のROMを問題なく起動できる場合、このROMにはコンソールの正確なグラフィックプラグインで実行できない無効または安全でないRSPマイクロコードが含まれている可能性があります。この場合、他のグラフィックプラグインを使用してROMを実行してみてください。</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="53"/>
        <source>The emulator exited shortly after launching. If you are able to launch other ROMs without issues, then this ROM is most likely corrupt.</source>
        <translation>起動後すぐにエミュレータが終了しました。他のROMを問題なく起動できる場合は、このROMが壊れている可能性が高いです。</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="266"/>
        <source>Emulator Missing</source>
        <translation>エミュレーターが見つかりません</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="267"/>
        <source>Failed to launch emulator. It does not appear to be installed.</source>
        <translation>エミュレータの起動に失敗しました。インストールされていないようです。</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="456"/>
        <source>Possible ROM Error</source>
        <translation>ロムエラーの可能性</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="526"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="532"/>
        <source>Do not warn me again</source>
        <translation>今後表示しない</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="605"/>
        <source>This game uses an emulated Gamecube controller, but you have not bound any inputs to the X and Y buttons.</source>
        <translation>このゲームではエミュレートされたゲームキューブコントローラーを使いますが、XボタンとYボタンに入力を割り当てていません。</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="615"/>
        <source>This game uses an emulated Gamecube controller, but you do not have an analog stick bound to the C buttons (C stick).</source>
        <translation>このゲームはエミュレートされたゲームキューブコントローラーを使用しますが、Cボタン（Cスティック）に割り当てられたスティックはありません。</translation>
    </message>
</context>
<context>
    <name>GetControllerPortDialog</name>
    <message>
        <location filename="../src/ui/get-controller-port-dialog.cpp" line="8"/>
        <source>Choose Controller</source>
        <translation>コントローラーを選択</translation>
    </message>
    <message>
        <location filename="../src/ui/get-controller-port-dialog.cpp" line="9"/>
        <source>Press any button on the controller to bind to this port.</source>
        <translation>コントローラのいずれかのボタンを押して、このポートに割り当てます。</translation>
    </message>
</context>
<context>
    <name>GroupName</name>
    <message>
        <location filename="../src/core/special-groups.cpp" line="14"/>
        <source>Favourites</source>
        <translation>お気に入り</translation>
    </message>
    <message>
        <location filename="../src/core/special-groups.cpp" line="16"/>
        <source>Uncategorized</source>
        <translation>未分類</translation>
    </message>
    <message>
        <location filename="../src/core/special-groups.cpp" line="18"/>
        <source>Want To Play</source>
        <translation>プレイしたい</translation>
    </message>
    <message>
        <location filename="../src/core/special-groups.cpp" line="20"/>
        <source>In Progress</source>
        <translation>進行中</translation>
    </message>
    <message>
        <location filename="../src/core/special-groups.cpp" line="22"/>
        <source>Completed</source>
        <translation>完了</translation>
    </message>
</context>
<context>
    <name>HotkeyEditWidget</name>
    <message>
        <location filename="../src/ui/hotkey-edit-widget.cpp" line="8"/>
        <source>None</source>
        <translation>なし</translation>
    </message>
</context>
<context>
    <name>ImportSaveDialog</name>
    <message>
        <location filename="../src/ui/import-save-dialog.cpp" line="10"/>
        <source>Select Save File</source>
        <translation>セーブファイルを選択</translation>
    </message>
    <message>
        <location filename="../src/ui/import-save-dialog.cpp" line="11"/>
        <source>Project64 Save Files</source>
        <translation>プロジェクト64セーブファイル</translation>
    </message>
    <message>
        <location filename="../src/ui/import-save-dialog.cpp" line="21"/>
        <source>Save data imported</source>
        <translation>セーブデータをインポートしました</translation>
    </message>
    <message>
        <location filename="../src/ui/import-save-dialog.cpp" line="24"/>
        <source>Failed to import save data</source>
        <translation>セーブデータのインポートに失敗しました</translation>
    </message>
</context>
<context>
    <name>ImportSdCardDialog</name>
    <message>
        <location filename="../src/ui/designer/import-sd-card-dialog.ui" line="17"/>
        <source>Import SD Card</source>
        <translation>SDカード挿入</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/import-sd-card-dialog.ui" line="31"/>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/import-sd-card-dialog.ui" line="69"/>
        <source>Use this file directly</source>
        <translation>このファイルを使う</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/import-sd-card-dialog.ui" line="79"/>
        <source>Make a copy of this file</source>
        <translation>このファイルのコピーを作成します</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="69"/>
        <source>Import</source>
        <translation>インポート</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="132"/>
        <source>This name is already in use</source>
        <translation>この名前は既に使われています</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="149"/>
        <source>This name is reserved by Windows</source>
        <translation>この名前はWindowsによって予約されています</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="165"/>
        <source>Unexpected Error</source>
        <translation>予期せぬエラー</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="165"/>
        <source>Failed to import SD card.</source>
        <translation>SDカードのインポートに失敗しました。</translation>
    </message>
</context>
<context>
    <name>InputModeSelect</name>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="514"/>
        <source>Normal</source>
        <translation>ノーマル</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="515"/>
        <source>Maps your gamepad inputs to a single N64 controller using your controller profile</source>
        <translation>コントローラプロファイルを使用して、ゲームパッド入力を1つのN64コントローラにマッピングします</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="538"/>
        <source>Dual Analog</source>
        <translation>デュアルアナログ</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="539"/>
        <source>Your gamepad inputs that normally bind to the C buttons instead bind to the analog stick on a second N64 controller</source>
        <translation>通常Cボタンにバインドされるゲームパッドの入力が、2つ目のN64コントローラのアナログスティックにバインドされる</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="562"/>
        <source>GoldenEye</source>
        <translation>ゴールデンアイ</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="563"/>
        <source>Maps your gamepad inputs to two N64 controllers suitable for playing GoldenEye with the 2.4 Goodhead control style</source>
        <translation>ゲームパッド入力を、2.4グッドヘッド・コントロール・スタイルでゴールデンアイをプレイするのに適した2つのN64コントローラにマッピングします</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="586"/>
        <source>Clone</source>
        <translation>クローン</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="587"/>
        <source>Your gamepad inputs are sent to two controller ports instead of just one</source>
        <translation>ゲームパッドの入力は、1つではなく2つのコントローラーポートに送られます</translation>
    </message>
</context>
<context>
    <name>IsViewerWindow</name>
    <message>
        <location filename="../src/ui/is-viewer-window.cpp" line="58"/>
        <source>Error: IS Viewer was disconnected from the emulator.</source>
        <translation>エラー: IS Viewerがエミュレータから切断されました。</translation>
    </message>
    <message>
        <location filename="../src/ui/is-viewer-window.cpp" line="59"/>
        <source>Error: Failed to connect IS Viewer to the emulator.</source>
        <translation>エラー: IS Viewerとエミュレータの接続に失敗しました。</translation>
    </message>
</context>
<context>
    <name>KeyboardConfigDialog</name>
    <message>
        <location filename="../src/ui/designer/keyboard-config-dialog.ui" line="14"/>
        <source>Configure Keyboard Controls</source>
        <translation>キーボードコントローラーの設定</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/keyboard-config-dialog.ui" line="28"/>
        <source>Hotkeys</source>
        <translation>ホットキー</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/keyboard-config-dialog.ui" line="71"/>
        <source>Keyboard Controls</source>
        <translation>キーボードコントローラー</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="18"/>
        <source>Analog Up</source>
        <translation>アナログ&#x3000;上</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="19"/>
        <source>Analog Down</source>
        <translation>アナログ&#x3000;下</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="20"/>
        <source>Analog Left</source>
        <translation>アナログ&#x3000;左</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="21"/>
        <source>Analog Right</source>
        <translation>アナログ&#x3000;右</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="22"/>
        <source>C Up</source>
        <translation>C&#x3000;上</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="23"/>
        <source>C Down</source>
        <translation>C&#x3000;下</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="24"/>
        <source>C Left</source>
        <translation>C&#x3000;左</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="25"/>
        <source>C Right</source>
        <translation>C&#x3000;右</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="26"/>
        <source>D-Pad Up</source>
        <translation>Dボタン&#x3000;上</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="27"/>
        <source>D-Pad Down</source>
        <translation>Dボタン&#x3000;下</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="28"/>
        <source>D-Pad Left</source>
        <translation>Dボタン&#x3000;左</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="29"/>
        <source>D-Pad Right</source>
        <translation>Dボタン&#x3000;右</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="30"/>
        <source>A Button</source>
        <translation>Aボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="31"/>
        <source>B Button</source>
        <translation>Bボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="32"/>
        <source>L Trigger</source>
        <translation>Lボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="33"/>
        <source>Z Trigger</source>
        <translation>Zボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="34"/>
        <source>R Trigger</source>
        <translation>Rボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="35"/>
        <source>Start Button</source>
        <translation>スタートボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="36"/>
        <source>X Button</source>
        <translation>Xボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="37"/>
        <source>Y Button</source>
        <translation>Yボタン</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="39"/>
        <source>Save State</source>
        <translation>状態をセーブ</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="40"/>
        <source>Load State</source>
        <translation>状態をロード</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="41"/>
        <source>Previous State</source>
        <translation>前のセーブステート</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="42"/>
        <source>Next State</source>
        <translation>次のセーブステート</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="43"/>
        <source>Previous Cheat</source>
        <translation>前のチート</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="44"/>
        <source>Next Cheat</source>
        <translation>次のチート</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="45"/>
        <source>Toggle Cheat</source>
        <translation>チートの切り替え</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="46"/>
        <source>Toggle FPS Display</source>
        <translation>FPSディスプレイの切り替え</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="47"/>
        <source>Pause/Unpause</source>
        <translation>ポーズ/ポーズ解除</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="48"/>
        <source>Frame Advance</source>
        <translation>フレームアドバンス</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="49"/>
        <source>Fast Forward (Hold)</source>
        <translation>早送り (ホールド)</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="50"/>
        <source>Fast Forward (Toggle)</source>
        <translation>早送り (トグル)</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="51"/>
        <source>Slow Motion (Hold)</source>
        <translation>スローモーション (ホールド)</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="52"/>
        <source>Slow Motion (Toggle)</source>
        <translation>スローモーション (トグル)</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="53"/>
        <source>Rewind*</source>
        <translation>巻き戻し*</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="54"/>
        <source>Quit Emulator</source>
        <translation>エミュレーターを終了する</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="55"/>
        <source>Hard Reset</source>
        <translation>ハードリセット</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="56"/>
        <source>Toggle Fullscreen</source>
        <translation>フルスクリーンの切り替え</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="57"/>
        <source>RetroArch Menu</source>
        <translation>レトロアーチメニュー</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="58"/>
        <source>Record Video</source>
        <translation>録画</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="59"/>
        <source>Record Inputs</source>
        <translation>録画入力</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="60"/>
        <source>Take Screenshot</source>
        <translation>スクリーンショットを取得する</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="61"/>
        <source>Raise Volume</source>
        <translation>音量を上げる</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="62"/>
        <source>Lower Volume</source>
        <translation>音量を下げる</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="63"/>
        <source>Mute/Unmute</source>
        <translation>ミュート/ミュート解除</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="64"/>
        <source>Grab Mouse</source>
        <translation>グラブマウス</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="98"/>
        <source>You must enable rewind functionality in the RetroArch menu to use this hotkey.</source>
        <translation>このホットキーを使用するには、RetroArchメニューで巻き戻し機能を有効にする必要があります。</translation>
    </message>
</context>
<context>
    <name>LogViewerDialog</name>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="14"/>
        <source>Log Viewer</source>
        <translation>ログビューワー</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="28"/>
        <source>Only show logs from this session</source>
        <translation>このセッションのログのみを表示する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="48"/>
        <source>Show timestamps</source>
        <translation>タイムスタンプを表示</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="60"/>
        <source>Visible Logs</source>
        <translation>表示するログ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="66"/>
        <source>Debug</source>
        <translation>デバッグ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="76"/>
        <source>Info</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="86"/>
        <source>Warn</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="96"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="106"/>
        <source>Fatal</source>
        <translation>致命的</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../src/main.cpp" line="232"/>
        <source>Installing RetroArch</source>
        <translation>RetroArchをインストール中</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="233"/>
        <source>Parallel Launcher will now install RetroArch.</source>
        <translation>Paralle LancherがRetroArchをインストールします。</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="242"/>
        <source>Fatal Error</source>
        <translation>致命的なエラー</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="243"/>
        <source>Failed to install RetroArch.</source>
        <translation>RetroArchのインストールに失敗しました。</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="64"/>
        <source>Refresh ROM List</source>
        <translation>ロムリストを更新</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="81"/>
        <source>Configure Controller</source>
        <translation>コントローラー設定</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="98"/>
        <source>Options</source>
        <translation>オプション&lt;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="128"/>
        <source>Searching for ROMs...</source>
        <translation>ロムを探してます...</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="185"/>
        <source>No ROMS have been found</source>
        <translation>ロムが見つかりませんでした</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="192"/>
        <location filename="../src/ui/designer/main-window.ui" line="333"/>
        <source>Manage ROM Sources</source>
        <translation>ロムの管理</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="236"/>
        <source>Search...</source>
        <translation>検索…</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="283"/>
        <source>Download</source>
        <translation>ダウンロード</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="300"/>
        <source>Play Multiplayer</source>
        <translation>マルチプレイヤー</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="311"/>
        <source>Play Singleplayer</source>
        <translation>シングルプレイヤー</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="328"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="338"/>
        <source>Configure Controllers</source>
        <translation>コントローラー設定</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="343"/>
        <source>Keyboard Controls and Hotkeys</source>
        <translation>キーボード操作とホットキー</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="346"/>
        <source>Configure keyboard controls and hotkeys</source>
        <translation>キーボード操作とホットキーの設定</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="351"/>
        <location filename="../src/ui/main-window.cpp" line="789"/>
        <source>Login to romhacking.com</source>
        <translation>romhacking.comにログイン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="356"/>
        <source>Logout of romhacking.com</source>
        <translation>romhacking.comからログアウト</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="361"/>
        <source>Disable romhacking.com integration</source>
        <translation>romhacking.comとの統合を無効にする</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="366"/>
        <source>Add Single ROM</source>
        <translation>ロムを追加</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="375"/>
        <source>Quit Parallel Launcher</source>
        <translation>Parallel Lancherを閉じる</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="380"/>
        <source>Open save file directory</source>
        <translation>セーブファイルフォルダを開く</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="385"/>
        <source>Open savestate directory</source>
        <translation>セーブステートフォルダを開く</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="390"/>
        <source>Open SD card directory</source>
        <translation>SDカードフォルダを開く</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="395"/>
        <source>Open app data directory</source>
        <translation>データフォルダを開く</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="400"/>
        <source>Open app config directory</source>
        <translation>設定フォルダを開く</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="405"/>
        <source>Open app cache directory</source>
        <translation>アプリのキャッシュディレクトリを開く</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="144"/>
        <source>Data Directories</source>
        <translation>データディレクトリ</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="631"/>
        <source>Select a ROM</source>
        <translation>ロムを選択</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="632"/>
        <source>ROM or Patch File</source>
        <translation>ロムかパッチファイル</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="657"/>
        <location filename="../src/ui/main-window.cpp" line="660"/>
        <location filename="../src/ui/main-window.cpp" line="663"/>
        <location filename="../src/ui/main-window.cpp" line="666"/>
        <source>Patch Failed</source>
        <translation>パッチの適用に失敗しました</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="657"/>
        <source>Failed to patch ROM. The .bps patch appears to be invalid.</source>
        <translation>ROMのパッチに失敗しました。.bpsパッチが無効のようです。</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="660"/>
        <source>Failed to patch ROM. The base ROM does not match what the patch expects.</source>
        <translation>ROM のパッチに失敗しました。ベースROMがパッチが予期するものと一致しません。</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="663"/>
        <source>Failed to patch ROM. The base rom required to patch is not known to Parallel Launcher.</source>
        <translation>ROMのパッチに失敗しました。パッチに必要なベースROMがParallel Launcherに知られていません。</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="666"/>
        <source>Failed to patch ROM. An error occurred while writing the patched ROM to disk.</source>
        <translation>ROM のパッチに失敗した。パッチを当てたROMをディスクに書き込む際にエラーが発生しました。</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="789"/>
        <source>Enable romhacking.com integration</source>
        <translation>romhacking.comとの統合を有効にする</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="828"/>
        <source>Confirm Disable</source>
        <translation>無効化の確認</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="828"/>
        <source>Are you sure you want to disable romhacking.com integration?</source>
        <translation>本当にromhacking.comとの統合を無効にしますか？</translation>
    </message>
</context>
<context>
    <name>ManageGroupsDialog</name>
    <message>
        <location filename="../src/ui/designer/manage-groups-dialog.ui" line="14"/>
        <source>Manage Groups</source>
        <translation>グループの管理</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-groups-dialog.ui" line="32"/>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-groups-dialog.ui" line="43"/>
        <source>Rename</source>
        <translation>名前の変更</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-groups-dialog.ui" line="54"/>
        <source>New</source>
        <translation>新規</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="55"/>
        <source>Confirm Delete</source>
        <translation>削除の確認</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="55"/>
        <source>Are you sure you want to delete this group?</source>
        <translation>このグループを削除しますか？</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="75"/>
        <source>Rename Group</source>
        <translation>グループ名を変更する</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="75"/>
        <source>Enter a new name for your group</source>
        <translation>新しいグループ名を入力してください</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="85"/>
        <source>Rename Failed</source>
        <translation>名前の変更に失敗しました</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="85"/>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="104"/>
        <source>A group with this name already exists.</source>
        <translation>このグループ名は既に存在します..</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="99"/>
        <source>Create Group</source>
        <translation>グループの作成</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="99"/>
        <source>Enter a name for your new group</source>
        <translation>新しいグループ名を入力してください</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="104"/>
        <source>Create Failed</source>
        <translation>作成に失敗しました</translation>
    </message>
</context>
<context>
    <name>ManageSdCardsDialog</name>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="14"/>
        <source>Manage Virtual SD Cards</source>
        <translation>仮想SDカードの管理</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="42"/>
        <source>Create New</source>
        <translation>新しく作成</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="53"/>
        <source>Import</source>
        <translation>インポート</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="111"/>
        <source>Size</source>
        <translation>サイズ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="118"/>
        <source>Format</source>
        <translation>形式です</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="147"/>
        <source>None</source>
        <translation>なし</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="168"/>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="211"/>
        <source>Reformat</source>
        <translation>リフォーマット</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="222"/>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="251"/>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="423"/>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="445"/>
        <source>Clone</source>
        <translation>クローン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="262"/>
        <source>Browse Files</source>
        <translation>ファイルを閲覧</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="295"/>
        <source>Create</source>
        <translation>作成</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="306"/>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="125"/>
        <source>Confirm Reformat</source>
        <translation>リフォーマットの確認</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="126"/>
        <source>Are you sure you want to reformat this virtual SD card? All data currently on it will be lost.</source>
        <translation>本当に仮想SDカードをリフォーマットしますか？全てのデータが失われます。</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="221"/>
        <source>Confirm Deletion</source>
        <translation>削除の確認</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="222"/>
        <source>Are you sure you want to delete this virtual SD card? This action cannot be undone.</source>
        <translation>本当にこの仮想SDカードを削除しますか？この操作は元に戻せません。</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="253"/>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="287"/>
        <source>Unexpected Error</source>
        <translation>予期せぬエラー</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="253"/>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="287"/>
        <source>Failed to create the virtual SD card because of an unknown error.</source>
        <translation>不明なエラーが発生したため、仮想SDカードの作成に失敗しました。</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="305"/>
        <source>Import SD Card</source>
        <translation>SDカードをインポート</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="306"/>
        <source>Raw Disk Image</source>
        <translation>ローディスクイメージ</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="314"/>
        <source>Invalid disk image</source>
        <translation>ディスクイメージが無効です</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="314"/>
        <source>The specified file is not a valid disk image.</source>
        <translation>指定されたファイルは有効なディスクイメージではありません。</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="317"/>
        <source>Disk image too large</source>
        <translation>ディスクイメージが大きすぎます</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="317"/>
        <source>The disk image could not be imported because it is larger than 2 TiB.</source>
        <translation>ディスクイメージが2TiBより大きいため、インポートできませんでした。</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="404"/>
        <source>Operation Failed</source>
        <translation>オペレーションに失敗しました</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="405"/>
        <source>Failed to mount the SD card image. For more detailed error information, go back to the main window and press F7 to view error logs.</source>
        <translation>SDカードイメージのマウントに失敗しました。より詳細なエラー情報については、メインウィンドウに戻り、F7キーを押してエラーログを表示してください。</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="419"/>
        <source>Clone SD Card</source>
        <translation>SDカードクローン</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="420"/>
        <source>Enter a name for the copied SD card:</source>
        <translation>コピーしたSDカードの名前を入力します:</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="522"/>
        <source>Confirm Close</source>
        <translation>クローズ確認</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="523"/>
        <source>All SD cards you have opened for browsing will be closed. Continue?</source>
        <translation>閲覧用に開いたSDカードはすべて閉じられます。続行しますか？</translation>
    </message>
</context>
<context>
    <name>MultiplayerControllerSelectDialog</name>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="14"/>
        <source>Select Controllers</source>
        <translation>コントローラーを選択</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="22"/>
        <source>Port 1</source>
        <translation>ポート１</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="33"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="59"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="85"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="111"/>
        <source>-- None --</source>
        <translation>〜 なし 〜</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="41"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="67"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="93"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="119"/>
        <source>Detect Device</source>
        <translation>デバイスの検出</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="48"/>
        <source>Port 2</source>
        <translation>ポート２</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="74"/>
        <source>Port 3</source>
        <translation>ポート３</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="100"/>
        <source>Port 4</source>
        <translation>ポート４</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="128"/>
        <source>Allow port 1 to save and load states</source>
        <translation>ポート1に状態のセーブとロードを許可する</translation>
    </message>
</context>
<context>
    <name>NeutralInputDialog</name>
    <message>
        <location filename="../src/ui/designer/neutral-input-dialog.ui" line="14"/>
        <source>Return to Neutral</source>
        <translation>ニュートラルに戻す</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/neutral-input-dialog.ui" line="29"/>
        <source>Return all triggers and analog sticks to their neutral position, then press any button on the controller or click the OK button to continue.</source>
        <translation>すべてのトリガーとアナログスティックをニュートラル位置に戻し、コントローラの任意のボタンを押すか、OKボタンをクリックして続行しま..</translation>
    </message>
</context>
<context>
    <name>NowPlayingWidget</name>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="34"/>
        <source>Now Playing: </source>
        <translation>プレイ中: </translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="106"/>
        <source>Play Time</source>
        <translation>プレイ時間</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="119"/>
        <source>Total</source>
        <translation>トータル</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="150"/>
        <source>This Session</source>
        <translation>このセッション</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="221"/>
        <source>Hard Reset</source>
        <translation>ハードリセット</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="232"/>
        <source>Kill Emulator</source>
        <translation>エミュレーターを終了する</translation>
    </message>
    <message>
        <location filename="../src/ui/now-playing-widget.cpp" line="18"/>
        <source>The hack author has not provided a star layout for this hack. This layout will show unattainable stars and may be missing some others.</source>
        <translation>このハックの制作者はスターディスプレイのレイアウトを提供していません。このレイアウトでは達成不可能な星が表示され、他の星が欠けている可能性があります。</translation>
    </message>
    <message>
        <location filename="../src/ui/now-playing-window.cpp" line="177"/>
        <source>Parallel Launcher - Now Playing</source>
        <translation>Paralle Launcher -プレイ中</translation>
    </message>
</context>
<context>
    <name>RetroArch</name>
    <message>
        <location filename="../src/core/retroarch.cpp" line="798"/>
        <source>[Parallel Launcher] Input driver encountered an unexpected error. Connected controllers may not be mapped correctly.</source>
        <translation>[パラレルランチャー] 入力ドライバで予期しないエラーが発生しました。接続されたコントローラが正しくマッピングされていない可能性があります。</translation>
    </message>
</context>
<context>
    <name>RhdcDownloadDialog</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-download-dialog.ui" line="14"/>
        <source>Downloading Patch</source>
        <translation>パッチをダウンロード中</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-download-dialog.ui" line="25"/>
        <source>Fetching hack info</source>
        <translation>ハック情報を取得</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="158"/>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="252"/>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="412"/>
        <source>Failed to download patch from RHDC</source>
        <translation>RHDCからのパッチのダウンロードに失敗しました</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="314"/>
        <source>Downloading star layout</source>
        <translation>スターレイアウトをダウンロード中</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="343"/>
        <source>Failed to download star layout from RHDC</source>
        <translation>RHDCからのスターレイアウトのダウンロードに失敗しました</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="363"/>
        <source>Downloading patch</source>
        <translation>パッチをダウンロード中</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="386"/>
        <source>Applying patch</source>
        <translation>パッチを適用中</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="395"/>
        <source>The rom could not be installed because you do not have an unmodified copy of the US version of Super Mario 64 in your known roms list.</source>
        <translation>US版スーパーマリオ64の未修正コピーがロムリストにないため、ロムをインストールできませんでした。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="452"/>
        <source>The rom could not be installed because no bps patch was found.</source>
        <translation>bpsパッチが見つからなかったため、ROMをインストールできませんでした。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="466"/>
        <source>Could not install &apos;%1&apos; because of an unexpected error while applying the patch</source>
        <translation>パッチの適用中に予期しないエラーが発生したため &apos;%1&apos; をインストールできませんでした</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="476"/>
        <source>Computing checksum</source>
        <translation>チェックサムの計算</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="498"/>
        <source>Romhack installed successfully</source>
        <translation>ロムハックのインストールが成功しました</translation>
    </message>
</context>
<context>
    <name>RhdcHackView</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="30"/>
        <source>Hack List:</source>
        <translation>ハックリスト:</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="63"/>
        <source>Sort By:</source>
        <translation>ソート:</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="80"/>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="85"/>
        <source>Popularity</source>
        <translation>人気</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="90"/>
        <source>Rating</source>
        <translation>レート</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="95"/>
        <source>Difficulty</source>
        <translation>難易度</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="100"/>
        <source>Last Played</source>
        <translation>最終プレイ</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="105"/>
        <source>Play Time</source>
        <translation>プレイ時間</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="110"/>
        <source>Stars</source>
        <translation>スター</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="115"/>
        <source>Fixed Shuffle</source>
        <extracomment>&quot;Fixed Shuffle&quot; means that the order of the roms is arbitrary, but will be consistent every time, as opposed to the &quot;Random Shuffle&quot; option which is randomized every time</extracomment>
        <translation>固定シャッフル</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="120"/>
        <source>Random Shuffle</source>
        <translation>ランダムシャッフル</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="141"/>
        <source>Search...</source>
        <translation>検索…</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="276"/>
        <source>Version</source>
        <translation>バージョン</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="89"/>
        <source>All Hack Lists</source>
        <translation>全てのハックリスト</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="140"/>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="146"/>
        <source>Authors:</source>
        <translation>制作者:</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="141"/>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="152"/>
        <source>Unknown</source>
        <extracomment>Shown when no authors are listed for a hack</extracomment>
        <translation>不明</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="146"/>
        <source>Author:</source>
        <translation>制作者:</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="313"/>
        <source>Create List</source>
        <translation>リストを作成</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="313"/>
        <source>Enter a name for your new hack list</source>
        <translation>新しいハックリスト名を入力</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="318"/>
        <source>List Exists</source>
        <translation>リストが存在する</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="318"/>
        <source>A hack list with this name already exists</source>
        <translation>この名前のハックリストはすでに存在します</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="344"/>
        <source>Rate Hack</source>
        <translation>ハックを評価</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="347"/>
        <source>Mark as Not Completed</source>
        <translation>未完了のマーク</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="349"/>
        <source>Mark as Completed</source>
        <translation>完了のマーク</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="359"/>
        <source>Move to Hack List...</source>
        <translation>ハックリストに移動する...</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="360"/>
        <source>Copy to Hack List...</source>
        <translation>ハックリストにコピーする...</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="367"/>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="368"/>
        <source>New List</source>
        <translation>新しいリスト</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="374"/>
        <source>Remove from &apos;%1&apos;</source>
        <translation>から削除 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="381"/>
        <source>Delete Save File</source>
        <translation>セーブファイルを削除</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="384"/>
        <source>Edit Save File</source>
        <translation>セーブファイルを編集</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="385"/>
        <source>Import Project64 Save File</source>
        <translation>Project64のセーブファイルをインポートする</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="386"/>
        <source>Show Save File</source>
        <translation>セーブファイルを表示</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="390"/>
        <source>Unfollow Hack</source>
        <translation>アンフォローハック</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="392"/>
        <source>Uninstall Hack Version</source>
        <translation>このハックのバージョンをアンインストール</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="487"/>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="523"/>
        <source>Confirm Delete</source>
        <translation>削除の確認</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="487"/>
        <source>Are you sure you want to delete your save file?</source>
        <translation>セーブファイルを削除しますか？</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="514"/>
        <source>Confirm Unfollow</source>
        <translation>フォロー解除の確認</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="514"/>
        <source>Are you sure you want to unfollow this hack? The ROM will not be deleted from your computer, but it will be removed from all of your hack lists on romhacking.com and your progress will no longer be synced with romhacking.com.</source>
        <translation>本当にこのハックのフォローを解除しますか？ROMはあなたのコンピューターから削除されませんが、romhacking.comの全てのハックリストから削除され、あなたの進行状況はromhacking.comと同期されなくなります。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="523"/>
        <source>Are you sure you want to delete this ROM file from your computer?</source>
        <translation>このROMファイルをコンピュータから削除しますか？</translation>
    </message>
</context>
<context>
    <name>RhdcHackWidget</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="302"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="341"/>
        <source>Popularity (Downloads)</source>
        <translation>人気 (ダウンロード数)</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="385"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="424"/>
        <source>Rating</source>
        <translation>レート</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="465"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="504"/>
        <source>Difficulty</source>
        <translation>難易度</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="561"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="600"/>
        <source>Last Played</source>
        <extracomment>Date and time that a hack was last played</extracomment>
        <translation>最終プレイ日時</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="641"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="680"/>
        <source>Total Play Time</source>
        <translation>総プレイ時間</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-widget.cpp" line="185"/>
        <source>Never</source>
        <extracomment>Text shown in place of a date when a hack hasn&apos;t been played yet</extracomment>
        <translation>未プレイ</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-widget.cpp" line="198"/>
        <source>Completed</source>
        <translation>コンプリート</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-widget.cpp" line="198"/>
        <source>Incomplete</source>
        <translation>未完</translation>
    </message>
</context>
<context>
    <name>RhdcLoginDialog</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="14"/>
        <source>RHDC Login</source>
        <translation>RHDC ログイン</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="66"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;To enable romhacking.com integration, you must have an unmodified copy of the US version of &lt;span style=&quot; font-style:italic;&quot;&gt;Super Mario 64&lt;/span&gt; in z64 format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;romhacking.comとの統合を有効にするには、US版の修正されていない &lt;span style=&quot; font-style:italic;&quot;&gt;Super Mario 64&lt;/span&gt; z64形式のコピーが必要です.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="128"/>
        <source>Browse for SM64 ROM</source>
        <translation>SM64のROMを選択する</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="180"/>
        <source>Sign in to romhacking.com</source>
        <translation>romhacking.comにサインインする</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="194"/>
        <source>Username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="204"/>
        <source>Password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="256"/>
        <source>Sign In</source>
        <translation>サインイン</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="359"/>
        <source>Multi-factor authentication is enabled on this account</source>
        <translation>このアカウントでは多要素認証が有効になっています</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="366"/>
        <source>Please enter the 6-digit authentication code from your authenticator app.</source>
        <translation>認証アプリから6桁の認証コードを入力してください。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="437"/>
        <source>Verify</source>
        <translation>確認する</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="501"/>
        <source>Incorrect authentication code</source>
        <translation>認証コードが正しくありません</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="546"/>
        <source>Please confirm your romhacking.com integration settings. You can always change these later in the settings dialog.</source>
        <translation>romhacking.comの統合設定を確認してください。後でいつでも設定ダイアログで変更できます。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="574"/>
        <source>Download Directory</source>
        <translation>ダウンロードフォルダ</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="596"/>
        <source>Browse</source>
        <translation>参照</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="620"/>
        <source>Sets the directory that hacks downloaded from romhacking.com will be saved to. If you change this setting later, your roms will automatically be moved to the new directory.</source>
        <translation>romhacking.comからダウンロードしたハックが保存されるディレクトリを設定します。後でこの設定を変更すると、ロムは自動的に新しいディレクトリに移動します。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="652"/>
        <source>Enable Star Display</source>
        <translation>スターディスプレイを有効にする</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="673"/>
        <source>If checked, when a rom is running that has a supported layout file from RHDC, a layout of all the stars in the hack will be shown. This layout updates automatically after the game is saved.</source>
        <translation>チェックを入れると、RHDCのレイアウトファイルがサポートされているロムを実行すると、ハック内のすべての星のレイアウトが表示されます。このレイアウトはゲームがセーブされると自動的に更新されます。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="710"/>
        <source>Show star display for hacks without a star layout</source>
        <translation>スターレイアウトのないハックにスターを表示する</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="731"/>
        <source>If checked, Parallel Launcher will still show a star display with a default layout if the hack author did not provide one.</source>
        <translation>チェックを入れると、ハック作成者がデフォルトのレイアウトを提供していない場合でも、Parallel Launcherはデフォルトのレイアウトで星を表示します。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="768"/>
        <source>Check all save slots</source>
        <translation>セーブスロットを全てチェック</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="786"/>
        <source>If checked, Parallel Launcher will submit your highest star count across all save slots to romhacking.com; otherwise, it will only submit your star count in slot A.</source>
        <translation>チェックを入れると、Parallel Launcherがすべてのセーブスロットで最も高いスター数をromhacking.comに送信します； そうでなければ スロットAのスター数のみを送信します。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="818"/>
        <source>Prefer HLE plugins</source>
        <translation>HLEプラグインを優先する</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="836"/>
        <source>If checked, Parallel Launcher will use the GLideN64 plugin instead of the ParaLLEl plugin when it guesses that ParrLLEl is probably the best plugin to use. Only check this if your computer has an old GPU with poor Vulkan support. If you still experience lag even with GLideN64, you may need to disable &apos;Emulate Framebuffer&apos; and/or &apos;Emulate N64 Depth Compare&apos; in the GFX Plugins section of your Parallel Launcher settings.</source>
        <translation>チェックを入れると、Parallel Launcherは、ParrLLElプラグインを使用するのが最適であると判断した場合、ParaLLElプラグインの代わりにGLideN64プラグインを使用します。お使いのコンピュータのGPUが古く、Vulkanのサポートが不十分な場合にのみ、このチェックを入れてください。GLideN64を使用してもラグが発生する場合は、Parallel Launcher設定のGFX Pluginsセクションで「Emulate Framebuffer」または「Emulate N64 Depth Compare」を無効にする必要があるかもしれません。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="868"/>
        <source>Ignore widescreen hint</source>
        <translation>ワイドスクリーンのヒントを無視する</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="886"/>
        <source>If checked, Parallel Launcher will always default to using 4:3 resolution, even if the recommended settings indicate that widescreen is supported</source>
        <translation>チェックを入れると、推奨設定でワイドスクリーンがサポートされている場合でも、パラレルランチャーは常にデフォルトで4:3の解像度を使用します</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="79"/>
        <source>Select SM64 ROM</source>
        <translation>SM64ロムを選択</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="85"/>
        <source>ROM Does not Match</source>
        <translation>ロムが一致しません</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="85"/>
        <source>The provided rom does not match the expected checksum.</source>
        <translation>ロムがチェックサムと一致しません。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="127"/>
        <source>Select a new download directory</source>
        <translation>新しいダウンロードフォルダを選択する</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="150"/>
        <source>Enter your username</source>
        <translation>ユーザ名を入力</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="155"/>
        <source>Enter your password</source>
        <translation>パスワードを入力</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="206"/>
        <source>Username or password is incorrect</source>
        <translation>ユーザ名かパスワードが誤っています</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="209"/>
        <source>The romhacking.com servers appear to be down.</source>
        <translation>romhacking.comがダウンしているようです。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="212"/>
        <source>An unknown error occurred.</source>
        <translation>不明なエラーが発生しました。</translation>
    </message>
</context>
<context>
    <name>RhdcRatingDialog</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-rating-dialog.ui" line="14"/>
        <source>Rate this Hack</source>
        <translation>このハックの評価</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-rating-dialog.ui" line="25"/>
        <source>How would you rate this hack?</source>
        <translation>このハックをどう評価しますか？</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-rating-dialog.ui" line="51"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Quality. &lt;/span&gt;How enjoyable, impressive, and/or polished was this hack?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Quality. &lt;/span&gt;このハックはどの程度楽しく、印象的で、洗練されていましたか?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-rating-dialog.ui" line="174"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Difficulty. &lt;/span&gt;In general, how would you rate this hack&apos;s difficulty level?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Difficulty. &lt;/span&gt;このハックの難易度をどう評価しますか?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="10"/>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="24"/>
        <source>I can&apos;t decide or have no opinion</source>
        <extracomment>0</extracomment>
        <translation>決められないし、意見もない</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="11"/>
        <source>No Challenge</source>
        <extracomment>1</extracomment>
        <translation>ノーチャレンジ</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="12"/>
        <source>Very Easy</source>
        <extracomment>2</extracomment>
        <translation>とても簡単</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="13"/>
        <source>Casual</source>
        <extracomment>3</extracomment>
        <translation>カジュアル</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="14"/>
        <source>Classic SM64</source>
        <extracomment>4</extracomment>
        <translation>クラシックなSM64</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="15"/>
        <source>Moderate</source>
        <extracomment>5</extracomment>
        <translation>中級</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="16"/>
        <source>Challenging</source>
        <extracomment>6</extracomment>
        <translation>チャレンジ</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="17"/>
        <source>Difficult</source>
        <extracomment>7</extracomment>
        <translation>難しい</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="18"/>
        <source>Very Difficult</source>
        <extracomment>8</extracomment>
        <translation>とても難しい</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="19"/>
        <source>Extremely Difficult</source>
        <extracomment>9</extracomment>
        <translation>極めて難しい</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="20"/>
        <source>Borderline Kaizo</source>
        <extracomment>10</extracomment>
        <translation>kaizoのボーダーライン</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="25"/>
        <source>Beginner/Introductory Kaizo</source>
        <extracomment>1</extracomment>
        <translation>初級/kaizo入門</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="26"/>
        <source>Easy Kaizo</source>
        <extracomment>2</extracomment>
        <translation>簡単なkaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="27"/>
        <source>Standard Kaizo</source>
        <extracomment>3</extracomment>
        <translation>基本的なkaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="28"/>
        <source>Moderate Kaizo</source>
        <extracomment>4</extracomment>
        <translation>中級程度のkaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="29"/>
        <source>Challenging Kaizo</source>
        <extracomment>5</extracomment>
        <translation>チャレンジkaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="30"/>
        <source>Difficult Kaizo</source>
        <extracomment>6</extracomment>
        <translation>難しいkaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="31"/>
        <source>Very Difficult Kaizo</source>
        <extracomment>7</extracomment>
        <translation>とても難しいkaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="32"/>
        <source>Extremely Difficult Kaizo</source>
        <extracomment>8</extracomment>
        <translation>極めて難しいkaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="33"/>
        <source>Hardest humanly possible Kaizo</source>
        <extracomment>9</extracomment>
        <translation>最上級のkaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="34"/>
        <source>TAS/Unbeatable</source>
        <extracomment>10</extracomment>
        <translation>TAS専用</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="45"/>
        <source>Submit</source>
        <translation>投稿する</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="76"/>
        <source>Not Rated</source>
        <translation>未評価</translation>
    </message>
</context>
<context>
    <name>RhdcSaveEditor</name>
    <message>
        <location filename="../src/rhdc/ui/rhdc-save-editor.cpp" line="92"/>
        <source>Save Editor</source>
        <translation>セーブエディター</translation>
    </message>
</context>
<context>
    <name>RhdcSaveSyncDialog</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="14"/>
        <source>Synchronize Save Files</source>
        <translation>セーブファイルの同期</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="32"/>
        <source>Save file synchronization is enabled for this hack; however, your save file progress is not currently in sync. Please select which save file you would like to use for this hack.</source>
        <translation>このハックではセーブファイルの同期が有効になっていますが、セーブファイルの進行状況は現在同期されていません。このハックで使用するセーブファイルを選択してください。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="67"/>
        <source>Version</source>
        <extracomment>Which version of the hack the save file is for</extracomment>
        <translation>バージョン</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="72"/>
        <source>Last Saved</source>
        <extracomment>When the save file was last modified</extracomment>
        <translation>最後にしたセーブ</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="77"/>
        <source>Stars</source>
        <extracomment>Number of stars collected on the save file</extracomment>
        <translation>スター</translation>
    </message>
</context>
<context>
    <name>RhdcSync</name>
    <message>
        <location filename="../src/rhdc/core/sync.cpp" line="235"/>
        <source>Failed to connect to romhacking.com</source>
        <translation>romhacking.comへの接続に失敗しました</translation>
    </message>
</context>
<context>
    <name>RhdcViewBubble</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-view-bubble.ui" line="171"/>
        <source>Click here to go to the romhacking.com view.</source>
        <translation>ここをクリックするとromhacking.comのページに移動します。</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-view-bubble.ui" line="214"/>
        <source>Okay</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>RomListModel</name>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="243"/>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="244"/>
        <source>Internal Name</source>
        <translation>内部名</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="245"/>
        <source>File Path</source>
        <translation>ファイルのパス</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="246"/>
        <source>Last Played</source>
        <translation>最終プレイ</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="247"/>
        <source>Play Time</source>
        <translation>プレイ時間</translation>
    </message>
</context>
<context>
    <name>RomListView</name>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="236"/>
        <source>Delete Save File</source>
        <translation>セーブファイルを削除</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="237"/>
        <source>[SM64] Edit Save File</source>
        <translation>[SM64]セーブファイルを編集する</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="240"/>
        <source>Import Project64 Save File</source>
        <translation>Project64のセーブファイルをインポートする</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="241"/>
        <source>Show Save File</source>
        <translation>セーブファイルを表示</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="246"/>
        <source>Download</source>
        <translation>ダウンロード</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="250"/>
        <source>Add to...</source>
        <translation>追加する...</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="257"/>
        <source>New Group</source>
        <translation>新しいグループ</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="261"/>
        <source>Remove from %1</source>
        <translation>から削除する %1</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="267"/>
        <source>Open Containing Folder</source>
        <translation>フォルダを開く</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="268"/>
        <source>Delete ROM</source>
        <translation>ロムを削除</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="270"/>
        <location filename="../src/ui/rom-list-view.cpp" line="373"/>
        <source>Rename</source>
        <translation>名前の変更</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="272"/>
        <source>Revert ROM Name</source>
        <translation>ロム名を元に戻す</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="277"/>
        <source>Rate Hack</source>
        <translation>ハックを評価</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="280"/>
        <source>Mark as Not Completed</source>
        <translation>未完了のマーク</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="282"/>
        <source>Mark as Completed</source>
        <translation>完了のマーク</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="305"/>
        <location filename="../src/ui/rom-list-view.cpp" line="364"/>
        <source>Confirm Delete</source>
        <translation>削除の確認</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="305"/>
        <source>Are you sure you want to delete your save file?</source>
        <translation>セーブファイルを削除しますか？</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="325"/>
        <source>Create Group</source>
        <translation>グループを作成</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="325"/>
        <source>Enter a name for your new group</source>
        <translation>新しいグループの名前を入力してください</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="331"/>
        <source>Group Exists</source>
        <translation>グループが存在します</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="331"/>
        <source>A group with this name already exists</source>
        <translation>この名前のグループは既に存在します</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="364"/>
        <source>This will completely remove the ROM file from your computer. Are you sure you want to delete this ROM?</source>
        <translation>ロムファイルがコンピューターから完全に削除されます。本当にこのロムを削除しますか？</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="373"/>
        <source>Enter a new name for your rom</source>
        <translation>ロムの新しい名前を入力してください</translation>
    </message>
</context>
<context>
    <name>RomSettingsWidget</name>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="20"/>
        <source>Sync save files between hack versions</source>
        <translation>ハックバージョン間のセーブファイルの同期</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="29"/>
        <source>Input Mode</source>
        <translation>入力モード</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="76"/>
        <source>SD Card</source>
        <translation>SDカード</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="156"/>
        <source>Unlocks the CPU to run at full speed, removing CPU-based lag you would encounter on console. This is almost always safe, but can rarely cause issues on certain ROMs.</source>
        <translation>CPUのロックを解除してフルスピードで動作させ、コンソールで発生するCPUベースのラグを取り除く。これはほぼ常に安全だが、特定のROMではまれに問題を引き起こすことがある。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="159"/>
        <location filename="../src/ui/rom-settings-widget.cpp" line="171"/>
        <source>Overclock CPU (Recommended)</source>
        <translation>オーバークロックCPU (推奨)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="175"/>
        <source>Alters the vertical interrupt timing. In most cases this either gives a slight performance boost or has no effect, but in some cases it can cause the game to run at the wrong framerate.</source>
        <translation>垂直割り込みのタイミングを変更します。ほとんどの場合、これはわずかなパフォーマンスブーストを与えるか、影響を与えませんが、場合によっては、ゲームが間違ったフレームレートで実行されることがあります。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="178"/>
        <source>Overclock VI</source>
        <translation>オーバークロック VI</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="191"/>
        <source>Overrides save type autodetection, and forces the use of 16kB EEPROM for save data. This breaks roms that use 4kB EEPROM, so only enable this for romhacks that require it.</source>
        <translation>セーブタイプの自動検出を無効にし、セーブデータに 16kB EEPROM を強制的に使用します。これは4kB EEPROMを使うROMを壊すので、それを必要とするROMハックでのみ有効にしてください。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="194"/>
        <source>Force 16kB EEPROM</source>
        <translation>強制 16kB EEPROM</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="207"/>
        <source>Use the interpreter core instead of the dynamic recompiler. Slower, but can be useful for debugging.</source>
        <translation>ダイナミック・リコンパイラの代わりにインタプリタ・コアを使う。処理速度は遅くなるが、デバッグには便利である。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="210"/>
        <source>Use Interpreter Core</source>
        <translation>インタープリターコアを使用する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="223"/>
        <source>Enable widescreen mode by stretching the video. Some ROMs have a widescreen mode that will allow the video to no longer be stretched when it is enabled.</source>
        <translation>ビデオを引き伸ばしてワイドスクリーンモードを有効にする。一部のROMにはワイドスクリーンモードがあり、これを有効にするとビデオの引き伸ばしが行われなくなる。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="226"/>
        <source>Widescreen (Stretched)</source>
        <translation>ワイドスクリーン (引き伸ばし)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="239"/>
        <source>Enable widescreen mode by forcing the game to use a widescreen viewport. You will experience visual artifacts on the sides of the screen unless you are playing a romhack specifically designed for this widescreen mode.</source>
        <translation>ワイドスクリーンビューポートを強制的に使用することで、ワイドスクリーンモードを有効にしてください。このワイドスクリーンモード用に特別にデザインされたロムハックをプレイしていない限り、スクリーンの両サイドにビジュアルアーチファクトが発生します。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="242"/>
        <source>Widescreen (Viewport Hack)</source>
        <translation>ワイドスクリーン (ビューポートハック)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="255"/>
        <source>Upscale textures drawn using the TEX_RECT command. This can cause visual artifacts in some games.</source>
        <translation>TEX_RECT コマンドを使用して描画されたテクスチャをアップスケールする。これにより、ゲームによってはビジュアルアーチファクトが発生することがあります。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="258"/>
        <source>Upscale TEXRECTs</source>
        <translation>アップスケール TEXRECTs</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="271"/>
        <source>Removes the black borders on the left and right sides of the video. Since these pixels are never rendered on real hardware, results will vary depending on the game.</source>
        <translation>映像の左右の黒枠を削除します。これらのピクセルは実際のハードウェアではレンダリングされないため、結果はゲームによって異なります。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="274"/>
        <source>Remove Black Borders</source>
        <translation>黒枠の削除</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="287"/>
        <source>Emulate the native framebuffer. This is required for some visual effects to work, but may cause lag on lower end GPUs</source>
        <translation>ネイティブフレームバッファをエミュレートします。これはいくつかのビジュアルエフェクトを動作させるために必要ですが、ローエンドのGPUではラグを引き起こす可能性があります</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="290"/>
        <source>Emulate Framebuffer</source>
        <translation>フレームバッファのエミュレート</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="306"/>
        <source>Greatly increases accuracy by rendering decals correctly, but may cause a loss in performance</source>
        <translation>デカールを正しくレンダリングすることで精度が大幅に向上するが、パフォーマンスが低下する可能性があります</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="309"/>
        <source>Emulate N64 Depth Compare</source>
        <translation>N64の奥行きをエミュレートする</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="325"/>
        <source>Allows roms with custom RSP microcode to be played, but causes visible seams in models due to plugin inaccuracies.</source>
        <translation>カスタムRSPマイクロコードでロムを再生できるようにするが、プラグインの不正確さによりモデルに目に見える継ぎ目が生じる。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="348"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Not enabling CPU overclocking is likely to result in a laggy experience. This option should only be used for speedruns or testing where you want to approximate console CPU lag.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;注意: &lt;/span&gt;CPUオーバークロックを有効にしないと、ラグが生じやすくなります。このオプションは、スピードランや、コンソールのCPUラグをおおよそ把握したいテストにのみ使用してください。.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="373"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning:&lt;/span&gt; The &lt;span style=&quot; font-style:italic;&quot;&gt;Emulate N64 Depth Compare&lt;/span&gt; option does not work on MacOS. Some things may not render correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;注意:&lt;/span&gt; &lt;span style=&quot; font-style:italic;&quot;&gt;N64の奥行きをエミュレートする&lt;/span&gt; オプションはMacOSでは動作しません。正しくレンダリングされない場合があります。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="398"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning:&lt;/span&gt; This ROM supports mouse input, but you do not have a hotkey assigned to &lt;span style=&quot; font-style:italic;&quot;&gt;Grab Mouse&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;注意:&lt;/span&gt; このROMはマウス入力をサポートしていますが、以下のホットキーは割り当てられていません &lt;span style=&quot; font-style:italic;&quot;&gt;グラブマウス&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="475"/>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="592"/>
        <source>Graphics Plugin</source>
        <translation>グラフィックプラグイン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="484"/>
        <location filename="../src/ui/rom-settings-widget.cpp" line="538"/>
        <source>ParaLLEl (Recommended - very accurate to console)</source>
        <translation>ParaLLEl (推奨 - コンソールに非常に正確)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="497"/>
        <location filename="../src/ui/rom-settings-widget.cpp" line="539"/>
        <source>GLideN64 (Recommended for lower end computers)</source>
        <translation>GLideN64 (ローエンドコンピュータに推奨)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="507"/>
        <location filename="../src/ui/rom-settings-widget.cpp" line="540"/>
        <source>OGRE (Needed by some older romhacks)</source>
        <translation>OGRE (いくつかの古いロムハックで必要)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="557"/>
        <source>gln64 (Obsolete)</source>
        <translation>gln64 (旧式)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="601"/>
        <source>ParaLLEl (Recommended)</source>
        <translation>ParaLLEl (推奨)</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="171"/>
        <source>Overclock CPU</source>
        <translation>オーバークロックCPU</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="330"/>
        <source>Show Fewer Plugins</source>
        <translation>より少ないプラグインを表示する</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="330"/>
        <source>Show More Plugins</source>
        <translation>他のプラグインを表示する</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="341"/>
        <source>This ROM supports mouse input. After launching the emulator, your mouse will be captured by the emulator. Press %1 if you need to free the mouse cursor.</source>
        <translation>このROMはマウス入力をサポートしています。エミュレータを起動すると、マウスがエミュレータに取り込まれます。マウスカーソルを解放する必要がある場合は、%1を押してください。</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="520"/>
        <source>(recommended by hack author)</source>
        <translation>(ハック制作者の推奨)</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="612"/>
        <source>None</source>
        <translation>なし</translation>
    </message>
</context>
<context>
    <name>RomSourceDialog</name>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="14"/>
        <source>Manage ROM Sources</source>
        <translation>ロムの管理</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="31"/>
        <source>ROM Search Folders</source>
        <translation>フォルダのロムを検索する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="68"/>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="82"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="77"/>
        <source>New Source</source>
        <translation>新しいソース</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="103"/>
        <source>ROM Source</source>
        <translation>ロムのソース</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="114"/>
        <source>Browse for a folder</source>
        <translation>フォルダを参照する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="121"/>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="431"/>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="502"/>
        <source>Browse</source>
        <translation>参照</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="134"/>
        <source>Also scan all subfolders</source>
        <translation>すべてのサブフォルダもスキャンする</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="137"/>
        <source>Recursive</source>
        <translation>リカーシブ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="168"/>
        <source>Ignore directories that begin with a period character</source>
        <translation>ピリオドで始まるディレクトリを無視する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="171"/>
        <source>Ignore hidden directories</source>
        <translation>隠しフォルダを無視する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="183"/>
        <source>Max Depth</source>
        <translation>最大深度</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="236"/>
        <source>Follow symbolic links to folders and ROMs</source>
        <translation>フォルダやROMへのシンボリックリンクをたどる</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="239"/>
        <source>Follow Symlinks</source>
        <translation>シンボリックリンクをたどる</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="256"/>
        <source>Automatically add to groups</source>
        <translation>自動的にグループに追加する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="289"/>
        <source>Manage Groups</source>
        <translation>グループの管理</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="317"/>
        <source>Individual ROMs</source>
        <translation>個別ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="348"/>
        <source>Forget Selected ROM</source>
        <translation>選択されたROMを管理から外す</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="359"/>
        <source>Add New ROM(s)</source>
        <translation>新しいロムを追加する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="388"/>
        <source>BPS Patches</source>
        <translation>BPSパッチ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="417"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="298"/>
        <source>Patch File</source>
        <translation>パッチファイル</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="444"/>
        <source>Base ROM</source>
        <translation>ベースロム</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="450"/>
        <source>Automatic (Search Existing ROMs)</source>
        <translation>自動 (既存のROMを検索)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="463"/>
        <source>Provided ROM:</source>
        <translation>ロムを選択する:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="532"/>
        <source>Apply Patch</source>
        <translation>パッチを適用</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="275"/>
        <source>Select one or more ROMs</source>
        <translation>1つ以上のROMを選択する</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="297"/>
        <source>Select a .bps patch file</source>
        <translation>bpsパッチを選択</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="308"/>
        <source>Select a ROM</source>
        <translation>ロムを選択</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="337"/>
        <source>Patch Applied</source>
        <translation>パッチが適用されました</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="337"/>
        <source>Saved patched rom to %1</source>
        <translation>パッチを適用したロムを %1 に保存しました</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="352"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="355"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="359"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="361"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="365"/>
        <source>Patch Failed</source>
        <translation>パッチの適用に失敗しました</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="352"/>
        <source>Failed to patch ROM. The .bps patch appears to be invalid.</source>
        <translation>ROMのパッチに失敗しました。.bpsパッチが無効のようです。</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="355"/>
        <source>Failed to patch ROM. The base ROM does not match what the patch expects.</source>
        <translation>ROMのパッチに失敗しました。ベースROMがパッチが予期するものと一致しません。</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="359"/>
        <source>Failed to patch ROM. The base rom required to patch is not known to Parallel Launcher.</source>
        <translation>ROMのパッチに失敗しました。パッチに必要なベースROMがParallel Launcherに知られていません。</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="361"/>
        <source>Failed to patch ROM. The base ROM is missing or invalid.</source>
        <translation>ROM のパッチに失敗しました。ベースROMが無いか無効です。</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="365"/>
        <source>Failed to patch ROM. An error occurred while writing the patched ROM to disk.</source>
        <translation>ROMのパッチに失敗しました。パッチを当てたROMをディスクに書き込む際にエラーが発生しました。</translation>
    </message>
</context>
<context>
    <name>SaveFileEditorDialog</name>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="14"/>
        <source>Edit SM64 Save File</source>
        <translation>SM64セーブファイルの編集</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;This is a save file editor for Super Mario 64 and SM64 romhacks. Attempting to use this save file editor with other ROMs will corrupt your save file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;警告: &lt;/span&gt;これは Super Mario 64 と SM64 romhacks 用のセーブファイルエディタです。 このセーブファイルエディターを他のROMで使用しようとすると、セーブファイルが壊れてしまいます。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="41"/>
        <source>Save Slot:</source>
        <translation>セーブスロット:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="55"/>
        <source>Slot A (Empty)</source>
        <translation>スロットA (空)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="60"/>
        <source>Slot B (Empty)</source>
        <translation>スロットB (空)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="65"/>
        <source>Slot C (Empty)</source>
        <translation>スロットC (空)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="70"/>
        <source>Slot D (Empty)</source>
        <translation>スロットD (空)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="95"/>
        <source>Delete Save Slot</source>
        <translation>セーブスロットの消去</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="106"/>
        <location filename="../src/ui/save-file-editor-dialog.cpp" line="62"/>
        <source>Edit Save Slot</source>
        <translation>セーブスロットの編集</translation>
    </message>
    <message>
        <location filename="../src/ui/save-file-editor-dialog.cpp" line="15"/>
        <source>Slot %1 (Empty)</source>
        <translation>スロット %1 (空)</translation>
    </message>
    <message>
        <location filename="../src/ui/save-file-editor-dialog.cpp" line="18"/>
        <source>Slot %1 (%2 Stars)</source>
        <translation>スロット %1 (%2 スター)</translation>
    </message>
    <message>
        <location filename="../src/ui/save-file-editor-dialog.cpp" line="66"/>
        <source>Create Save Slot</source>
        <translation>セーブスロットの作成</translation>
    </message>
</context>
<context>
    <name>SaveSlotEditorDialog</name>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="14"/>
        <source>Edit Save Slot (SM64)</source>
        <translation>セーブスロットの編集 (SM64)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="20"/>
        <source>Show flags and stars unused in the vanilla game</source>
        <translation>通常では使われていないフラグとスターを表示する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="32"/>
        <source>Flags</source>
        <translation>フラグ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="74"/>
        <source>Caps Unlocked</source>
        <translation>帽子のロック解除</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="80"/>
        <source>Wing Cap</source>
        <translation>羽根ぼうし</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="93"/>
        <source>Metal Cap</source>
        <translation>メタルぼうし</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="106"/>
        <source>Vanish Cap</source>
        <translation>スケスケぼうし</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="122"/>
        <source>Keys Collected</source>
        <translation>鍵を取得</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="128"/>
        <source>Basement Key</source>
        <translation>地下扉への鍵</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="141"/>
        <source>Upstairs Key</source>
        <translation>２階扉への鍵</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="157"/>
        <source>Unlocked Doors</source>
        <translation>ドアのロックを解放</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="163"/>
        <source>Basement Key Door</source>
        <translation>地下の鍵付きドア</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="176"/>
        <source>Upstairs Key Door</source>
        <translation>２階への鍵付きドア</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="189"/>
        <source>Peach&apos;s Secret Slide Door</source>
        <translatorcomment>The official Japanese translation for the stage is &quot;ピーチのかくれスライダー&quot;.</translatorcomment>
        <translation>ピーチのかくれスライダーのドア</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="202"/>
        <source>Whomp&apos;s Fortress Door</source>
        <translatorcomment>Official Japanese translation for the stage is &quot;バッタンキングの とりで&quot;</translatorcomment>
        <translation>バッタンキングのとりでのドア</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="215"/>
        <source>Cool Cool Mountain Door</source>
        <translatorcomment>Official Japanese translation for the stage name is &quot;さむいさむい マウンテン&quot;</translatorcomment>
        <translation>さむいさむいマウンテンのドア</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="228"/>
        <source>Jolly Rodger Bay Door</source>
        <translatorcomment>Official Japanese translation for the stage is &quot;かいぞくの いりえ&quot;</translatorcomment>
        <translation>かいぞくのいりえのドア</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="241"/>
        <source>Dark World Door</source>
        <translatorcomment>Dark World = &quot;やみの せかい&quot; .Just add translation for door</translatorcomment>
        <translation>８枚扉</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="254"/>
        <source>Fire Sea Door</source>
        <translatorcomment>Fire Sea = &quot;ほのおの うみ&quot;. Just add translation for &quot;Door&quot;</translatorcomment>
        <translation>３０枚扉</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="267"/>
        <source>50 Star Door</source>
        <translation>50枚扉</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="283"/>
        <source>Miscellaneous</source>
        <translation>その他</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="289"/>
        <source>Moat Drained</source>
        <translation>堀の水抜き</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="302"/>
        <source>Bowser&apos;s Sub Gone</source>
        <translation>クッパの潜水艦</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="318"/>
        <source>Unused</source>
        <translation>未設定</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="366"/>
        <source>Lost/Stolen Cap</source>
        <translation>失う/盗まれる 帽子</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="374"/>
        <source>Level</source>
        <translation>レベル</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="395"/>
        <source>Area</source>
        <translation>エリア</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="427"/>
        <source>Cap Stolen By:</source>
        <translation>帽子が盗まれる:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="434"/>
        <source>The flag indicates that Mario&apos;s cap is on the ground, however, when loading a save file, a cap that is on the ground is either given back to Mario or moved to the appropriate NPC for the area, so it will never actually be on the ground.</source>
        <translation>このフラグはマリオのキャップが地上にあることを示していますが、セーブファイルをロードする際、地上にあるキャップはマリオに戻されるか、そのエリアに適したNPCに移動されるため、実際には地上にあることはありません。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="437"/>
        <source>Automatic*</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="450"/>
        <source>Klepto (Bird)</source>
        <translatorcomment>Klepto is named &quot;ジャンゴ&quot; in the Japanese version</translatorcomment>
        <translation>ジャンゴ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="463"/>
        <source>Ukiki (Monkey)</source>
        <translatorcomment>Named ウッキィ in the Japanese version</translatorcomment>
        <translation>ウッキィ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="476"/>
        <source>Mr. Blizzard (Snowman)</source>
        <translatorcomment>Named &quot;スローマン&quot; in the Japanese version</translatorcomment>
        <translation>スローマン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="499"/>
        <source>Stars and Coins</source>
        <translation>スターとコイン</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="44"/>
        <source>Big Boo&apos;s Haunt</source>
        <translation>テレサの ホラーハウス</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="45"/>
        <source>Cool Cool Mountain</source>
        <translation>さむいさむい マウンテン</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="46"/>
        <source>Castle Interior</source>
        <translation>城内</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="47"/>
        <source>Hazy Maze Cave</source>
        <translation>やみにとける どうくつ</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="48"/>
        <source>Shifting Sand Land</source>
        <translation>あっちっち さばく</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="49"/>
        <source>Bob-Omb Battlefield</source>
        <translation>ボムへいの せんじょう</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="50"/>
        <source>Snowman&apos;s Land</source>
        <translation>スノーマンズ ランド</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="51"/>
        <source>Wet Dry World</source>
        <translation>みずびたシティー</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="52"/>
        <source>Jolly Rodger Bay</source>
        <translation>かいぞくの いりえ</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="53"/>
        <source>Tiny-Huge Island</source>
        <translation>ちびでか アイランド</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="54"/>
        <source>Tick Tock Clock</source>
        <translation>チックタックロック</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="55"/>
        <source>Rainbow Ride</source>
        <translation>レインボー クルーズ</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="56"/>
        <source>Castle Grounds</source>
        <translation>城外</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="57"/>
        <source>Bowser in the Dark World</source>
        <translation>やみの せかいの クッパ</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="58"/>
        <source>Vanish Cap Under the Moat</source>
        <translation>おほりのとうめいスイッチ</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="59"/>
        <source>Bowser in the Fire Sea</source>
        <translation>ほのおの うみの クッパ</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="60"/>
        <source>Secret Aquarium</source>
        <translation>おさかなと いっしょ</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="61"/>
        <source>Bowser in the Sky</source>
        <translation>てんくうの たたかい！</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="62"/>
        <source>Lethal Lava Land</source>
        <translation>ファイアバブル ランド</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="63"/>
        <source>Dire Dire Docks</source>
        <translation>ウォーター ランド</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="64"/>
        <source>Whomp&apos;s Fortress</source>
        <translation>バッタンキングの とりで</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="65"/>
        <source>End Screen</source>
        <translation>エンドスクリーン</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="66"/>
        <source>Castle Courtyard</source>
        <translation>城の中庭</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="67"/>
        <source>Peach&apos;s Secret Slide</source>
        <translation>ピーチのかくれスライダー</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="68"/>
        <source>Cavern of the Metal Cap</source>
        <translation>メタルスイッチの たき</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="69"/>
        <source>Tower of the Wing Cap</source>
        <translation>はばたけ！はねスイッチへ</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="70"/>
        <source>Bowser 1</source>
        <translation>クッパ 1</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="71"/>
        <source>Winged Mario over the Rainbow</source>
        <translation>にじ かける はねマリオ</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="73"/>
        <source>Bowser 2</source>
        <translation>クッパ 2</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="74"/>
        <source>Bowser 3</source>
        <translation>クッパ 3</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="76"/>
        <source>Tall, Tall Mountain</source>
        <translation>たかいたかいマウンテン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="11"/>
        <source>8 Red Coins</source>
        <translation>８枚赤コイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="12"/>
        <source>100 Coins</source>
        <translation>１００枚コイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="17"/>
        <source>Toad 1</source>
        <translation>キノピオ 1</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="18"/>
        <source>Toad 2</source>
        <translation>キノピオ 2</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="19"/>
        <source>Toad 3</source>
        <translation>キノピオ 3</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="20"/>
        <source>MIPS 1</source>
        <translation>ミップ 1</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="21"/>
        <source>MIPS 2</source>
        <translation>ミップ 2</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="27"/>
        <source>Big Bob-Omb on the Summit</source>
        <translation>やまのうえの ボムキング</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="28"/>
        <source>Footface with Koopa the Quick</source>
        <translation>はくねつ ノコノコレース</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="29"/>
        <source>Shoot to the Island in the Sky</source>
        <translation>そらのしままで ぶっとべ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="30"/>
        <source>Find the 8 Red Coins</source>
        <translation>８まいの あかコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="31"/>
        <source>Mario Wings to the Sky</source>
        <translation>そらにはばたけ はねマリオ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="32"/>
        <source>Behind Chain Chomp&apos;s Gate</source>
        <translation>ワンワンの いぬごやで</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="37"/>
        <source>Chip Off Whomp&apos;s Block</source>
        <translation>いかりのバッタン キング</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="38"/>
        <source>To the Top of the Fortress</source>
        <translation>とりでの てっぺんへ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="39"/>
        <source>Shoot into the Wild Blue</source>
        <translation>たいほうで ひとっとび</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="40"/>
        <source>Red Coins on the Floating Isle</source>
        <translation>うきしまの 8まいコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="41"/>
        <source>Fall onto the Caged Island</source>
        <translation>とりかごへ ストーン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="42"/>
        <source>Blast Away the Wall</source>
        <translation>たいほうで ぶっこわせ！</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="47"/>
        <source>Plunder in the Sunken Ship</source>
        <translation>ちんぼつせんの おたから</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="48"/>
        <source>Can the Eel Come Out to Play?</source>
        <translation>でてこい きょだいウツボ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="49"/>
        <source>Treasure of the Ocean Cave</source>
        <translation>かいていどうくつの おたから</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="50"/>
        <source>Red Coins on the Ship Afloat</source>
        <translation>うかんだフネの あかコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="51"/>
        <source>Blast to the Stone Pillar</source>
        <translation>いわのはしらへ ひとっとび</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="52"/>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="110"/>
        <source>Through the Jet Stream</source>
        <translation>ふきだす みずを くぐれ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="57"/>
        <source>Slip Slidin&apos; Away</source>
        <translation>スーパースノースライダー</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="58"/>
        <source>Li&apos;l Penguin Lost</source>
        <translation>まいごの こペンギン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="59"/>
        <source>Big Penguin Race</source>
        <translation>ペンギン チャンピオンレース</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="60"/>
        <source>Frosty Slide for 8 Red Coins</source>
        <translation>すべって8まい あかコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="61"/>
        <source>Snowman&apos;s Lost his Head</source>
        <translation>ゴロゴロ ゆきダルマ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="62"/>
        <source>Wall Kicks Will Work</source>
        <translation>かくれ スーパー カベキック</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="67"/>
        <source>Go on a Ghost Hunt</source>
        <translation>おやかたテレサを さがせ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="68"/>
        <source>Ride Big Boo&apos;s Merry-Go-Round</source>
        <translation>テレサの メリーゴーランド</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="69"/>
        <source>Secret of the Haunted Books</source>
        <translation>ほんだなオバケの ナゾ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="70"/>
        <source>Seek the 8 Red Coins</source>
        <translation>8まいコインは どこだ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="71"/>
        <source>Big Boo&apos;s Balcony</source>
        <translation>バルコニーの ボステレサ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="72"/>
        <source>Eye to Eye in the Secret Room</source>
        <translation>かくしべやの おおめだま</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="77"/>
        <source>Swimming Beast in the Cavern</source>
        <translation>ドッシーのいる ちていこ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="78"/>
        <source>Elevate for 8 Red Coins</source>
        <translation>そうさリフトの あかコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="79"/>
        <source>Metal-Head Mario Can Move!</source>
        <translation>メタルで ダッシュ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="80"/>
        <source>Navigating the Toxic Maze</source>
        <translation>ケムリめいろを ぬけて</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="81"/>
        <source>A-maze-ing Emergency Exit</source>
        <translation>ケムリめいろの ひじょうぐち</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="82"/>
        <source>Watch for Rolling Rocks</source>
        <translation>ゴロゴロいわの ひみつ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="87"/>
        <source>Boil the Big Bully</source>
        <translation>おとせ ボスどんけつ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="88"/>
        <source>Bully the Bullies</source>
        <translation>たたかえ！どんけつたい</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="89"/>
        <source>8-Coin Puzzle with 15 Pieces</source>
        <translation>15パズルの 8まいコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="90"/>
        <source>Red-Hot Log Rolling</source>
        <translation>コロコロ まるたわたり</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="91"/>
        <source>Hot-Foot-it into the Volcano</source>
        <translation>かざんの パワースター</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="92"/>
        <source>Elevator Tour in the Volcano</source>
        <translation>かざんの リフトツアー</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="97"/>
        <source>In the Talons of the Big Bird</source>
        <translation>いたずらハゲたか ジャンゴ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="98"/>
        <source>Shining Atop the Pyramid</source>
        <translation>ピラミッドの てっぺんで</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="99"/>
        <source>Inside the Ancient Pyramid</source>
        <translation>きょだいピラミッドの ないぶ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="100"/>
        <source>Stand Tall on the Four Pillars</source>
        <translation>4つのはしらに たつものへ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="101"/>
        <source>Free Flying for 8 Red Coins</source>
        <translation>とびまわれ 8まいコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="102"/>
        <source>Pyramid Puzzle</source>
        <translation>きょだいピラミッドのナゾ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="107"/>
        <source>Board Bowser&apos;s Sub</source>
        <translation>クッパの せんすいかん</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="108"/>
        <source>Chests in the Current</source>
        <translation>うずしおの たからばこ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="109"/>
        <source>Pole-Jumping for Red Coins</source>
        <translation>にげたクッパの あかコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="111"/>
        <source>The Manta Ray&apos;s Reward</source>
        <translation>マンタの おくりもの</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="112"/>
        <source>Collect the Caps...</source>
        <translation>ボウシが そろったら</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="117"/>
        <source>Snowman&apos;s Big Head</source>
        <translation>おおゆきダルマの おでこ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="118"/>
        <source>Chill with the Bully</source>
        <translation>こおりのくにの どんけ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="119"/>
        <source>In the Deep Freeze</source>
        <translation>こおりの オブジェ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="120"/>
        <source>Whirl from the Freezing Pond</source>
        <translation>つめたい いけを こえて</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="121"/>
        <source>Shell Shreddin&apos; for Red Coins</source>
        <translation>コウラにのってあかコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="122"/>
        <source>Into the Igloo</source>
        <translation>フワフワさんの おうち</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="127"/>
        <source>Shocking Arrow Lifts!</source>
        <translation>ビリビリの まめリフト</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="128"/>
        <source>Top o&apos; the Town</source>
        <translation>トップオブ ザ シティー</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="129"/>
        <source>Secrets in the Shallows &amp; Sky</source>
        <translation>あさせと そらのシークレット</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="130"/>
        <source>Express Evelator--Hurry Up!</source>
        <translation>いそげ！かなあみエレベーター</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="131"/>
        <source>Go to Town for Red Coins</source>
        <translation>ダウンタウンの あかコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="132"/>
        <source>Quick Race through Downtown!</source>
        <translation>ダウンタウンを かけろ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="137"/>
        <source>Scale the Mountain</source>
        <translation>たかいたかい やまのうえ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="138"/>
        <source>Mystery of the Monkey&apos;s Cage</source>
        <translation>いたずらザル ウッキィのおり</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="139"/>
        <source>Scary &apos;Shrooms, Red Coins</source>
        <translation>おばけキノコの あかコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="140"/>
        <source>Mysterious Mountainside</source>
        <translation>マウンテン スライダー</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="141"/>
        <source>Breathtaking View from Bridge</source>
        <translation>はしのうえから みわたせば</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="142"/>
        <source>Blast to the Lonely Mushroom</source>
        <translation>ぶっとべ はなれキノコへ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="147"/>
        <source>Pluck the Piranha Flower</source>
        <translation>きょだいパックンフラワー</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="148"/>
        <source>The Tip Top of the Huge Island</source>
        <translation>デカじまの てっぺんで</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="149"/>
        <source>Rematch with Koopa the Quick</source>
        <translation>ノコノコ リターンマッチ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="150"/>
        <source>Five Itty Bitty Secrets</source>
        <translation>チビじまの 5シークレット</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="151"/>
        <source>Wiggler&apos;s Red Coins</source>
        <translation>ハナチャンの あかコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="152"/>
        <source>Make Wiggler Squirm</source>
        <translation>イカリの ハナチャン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="157"/>
        <source>Roll into the Cage</source>
        <translation>グルグル かごのなかへ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="158"/>
        <source>The Pit and the Pendulums</source>
        <translation>ふりこの へや</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="159"/>
        <source>Get a Hand</source>
        <translation>チックタックはりの うえ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="160"/>
        <source>Stomp on the Thwomp</source>
        <translation>えっへん てっぺん ドッスン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="161"/>
        <source>Timed Jumps on Moving Bars</source>
        <translation>つきだしを のりこえて</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="162"/>
        <source>Stop Time for Red Coins</source>
        <translation>00ふんの あかコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="167"/>
        <source>Cruiser Crossing the Rainbow</source>
        <translation>にじを わたる ふね</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="168"/>
        <source>The Big House in the Sky</source>
        <translation>てんくうの おやしき</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="169"/>
        <source>Coins Amassed in a Maze</source>
        <translation>タテめいろの あかコイン</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="170"/>
        <source>Swingin&apos; in the Breeze</source>
        <translation>かぜきる きょだいブランコ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="171"/>
        <source>Tricky Triangles!</source>
        <translation>おおぞら アスレチック</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="172"/>
        <source>Somewhere Over the Rainbow</source>
        <translation>にじの かなたの しま</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="227"/>
        <source>Act 1 Star</source>
        <translation>スター選択１</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="228"/>
        <source>Act 2 Star</source>
        <translation>スター選択２</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="229"/>
        <source>Act 3 Star</source>
        <translation>スター選択３</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="230"/>
        <source>Act 4 Star</source>
        <translation>スター選択４</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="231"/>
        <source>Act 5 Star</source>
        <translation>スター選択５</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="232"/>
        <source>Act 6 Star</source>
        <translation>スター選択６</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="233"/>
        <source>100 Coin Star</source>
        <translation>１００枚コインスター</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="234"/>
        <source>Cannon Status</source>
        <translation>大砲のフラグ</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="235"/>
        <source>Coin High Score</source>
        <translation>コイン&#x3000;ハイスコア</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="264"/>
        <source>Peach&apos;s Castle</source>
        <translation>ピーチ城</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="317"/>
        <source>-- None --</source>
        <translation>〜 なし 〜</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="14"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="44"/>
        <source>User Interface</source>
        <translation>ユーザーインターフェース</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="49"/>
        <source>Emulation</source>
        <translation>エミュレーション</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="54"/>
        <source>BPS Patches</source>
        <translation>bpsパッチ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="59"/>
        <source>GFX Plugins</source>
        <translation>GFXプラグイン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="64"/>
        <source>Updaters</source>
        <translation>アップデート</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="69"/>
        <source>Romhacking.com</source>
        <translation>Romhacking.com</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="74"/>
        <source>Developer</source>
        <translation>開発者</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="79"/>
        <source>System Clock</source>
        <translation>システムクロック</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="118"/>
        <source>Theme (Requires Restart)</source>
        <translation>テーマ (再起動が必要)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="134"/>
        <source>Dark Mode (Requires Restart)</source>
        <translation>ダークモード (再起動が必要)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="143"/>
        <source>Language (Requires Restart)</source>
        <translation>言語(再起動が必要)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="158"/>
        <source>Hide launcher while playing ROM</source>
        <translation>プレイ中はLauncherを隠す</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="165"/>
        <source>Check for updates on startup</source>
        <translation>起動時にアップデートを確認する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="175"/>
        <source>Discord &quot;Now Playing&quot; Integration</source>
        <translation>ディスコードの &quot;プレイ中&quot; を統合</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="182"/>
        <source>Show additional rom settings meant for advanced users</source>
        <translation>上級ユーザー向けの追加ロム設定を表示</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="185"/>
        <source>Show advanced ROM options</source>
        <translation>高度なROMオプションを表示する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="192"/>
        <source>Visible Columns</source>
        <translation>カラム</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="198"/>
        <source>The full path to the file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="201"/>
        <source>File Path</source>
        <translation>ファイルのパス</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="211"/>
        <source>The filename without the extension</source>
        <translation>拡張子を除いたファイル名</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="214"/>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="227"/>
        <source>The name stored in the ROM itself</source>
        <translation>ROM本体に格納されている名前</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="230"/>
        <source>Internal Name</source>
        <translation>内部名</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="237"/>
        <source>The date you last played the ROM</source>
        <translation>最後にROMをプレイした日付</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="240"/>
        <source>Last Played</source>
        <translation>最終プレイ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="250"/>
        <source>The total time spent playing the ROM</source>
        <translation>ROMの総プレイ時間</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="253"/>
        <source>Total Play Time</source>
        <translation>総プレイ時間</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="385"/>
        <source>Default Emulator Core</source>
        <translation>デフォルトのエミュレーターコア</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="409"/>
        <source>Default ParallelN64 Plugin</source>
        <translation>デフォルトのParallelN64プラグイン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="438"/>
        <source>Window Scale</source>
        <translation>ウィンドウスケール</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="448"/>
        <source>Default Mupen Plugin</source>
        <translation>デフォルトのMupenプラグイン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="458"/>
        <source>Audio Driver</source>
        <translation>オーディオドライバー</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="486"/>
        <source>Fullscreen</source>
        <translation>フルスクリーン</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="493"/>
        <source>Vsync</source>
        <translation>垂直同期</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="500"/>
        <source>Pause when emulator loses focus</source>
        <translation>エミュレータがフォーカスを失ったときに一時停止する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="507"/>
        <source>Do not show RetroArch notifications such as &quot;Saved state to slot #0&quot; or &quot;Device disconnected from port #1&quot;</source>
        <translation>この様なRetroArchの通知を表示しない &quot;スロットに状態を保存 #0&quot; や &quot;デバイスがポートから切断されました #1&quot;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="510"/>
        <source>Hide notifications</source>
        <translation>通知を隠す</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="527"/>
        <source>Emulate SummerCart64 SD card interface</source>
        <translation>SummerCart64のSDカードインターフェースをエミュレート</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="542"/>
        <source>Reset RetroArch Config</source>
        <translation>RetroArchの設定をリセット</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="594"/>
        <source>When loading a BPS patch, save the patched ROM...</source>
        <translation>BPSパッチをロードする際、パッチを適用したROMを保存する...</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="601"/>
        <source>To the same folder as the patch</source>
        <translation>パッチと同じフォルダ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="614"/>
        <source>To this folder:</source>
        <translation>フォルダを指定:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="656"/>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1355"/>
        <source>Browse</source>
        <translation>参照</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="691"/>
        <source>Global Settings</source>
        <translation>グローバル設定</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="715"/>
        <source>Upscaling</source>
        <translation>拡大</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="731"/>
        <source>Native (x1 - 320x240)</source>
        <translation>固有 (x1 - 320x240)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="769"/>
        <location filename="../src/ui/designer/settings-dialog.ui" line="898"/>
        <source>Anti-Aliasing</source>
        <translation>アンチエイリアス</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="804"/>
        <source>Filtering</source>
        <translation>フィルタリング</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="815"/>
        <source>None</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="820"/>
        <source>Anti-Alias</source>
        <translation>アンチエイリアス</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="825"/>
        <source>Anti-Alias + Dedither</source>
        <translation>アンチエイリアス + デディザー</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="830"/>
        <source>Anti-Alias + Blur</source>
        <translation>アンチエイリアス + ぼかし</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="835"/>
        <source>Filtered</source>
        <translation>フィルタリング出力モード</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="908"/>
        <source>Use N64 3-Point Filtering</source>
        <translation>N64 3ポイント フィルタリングを使用する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="932"/>
        <source>Default ROM Settings</source>
        <translation>デフォルトのロム設定</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="956"/>
        <source>Upscale textures drawn using the TEX_RECT command. This can cause visual artifacts in some games.</source>
        <translation>TEX_RECT コマンドを使用して描画されたテクスチャをアップスケールする。これにより、ゲームによってはビジュアルアーチファクトが発生することがあります。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="959"/>
        <source>Upscale TEXRECTs</source>
        <translation>アップスケールTEXRECTs</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="979"/>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1101"/>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1145"/>
        <source>Apply to all existing ROMs</source>
        <translation>すべての既存ROMに適用</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="997"/>
        <source>Removes the black borders on the left and right sides of the video. Since these pixels are never rendered on real hardware, results will vary depending on the game.</source>
        <translation>映像の左右の黒枠を削除します。これらのピクセルは実際のハードウェアではレンダリングされないため、結果はゲームによって異なります。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1000"/>
        <source>Remove Black Borders</source>
        <translation>黒枠を削除する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1075"/>
        <source>Emulate the native framebuffer. This is required for some visual effects to work, but may cause lag on lower end GPUs</source>
        <translation>ネイティブフレームバッファをエミュレートします。これはいくつかのビジュアルエフェクトを動作させるために必要ですが、ローエンドのGPUではラグを引き起こす可能性があります</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1078"/>
        <source>Emulate Framebuffer</source>
        <translation>フレームバッファのエミュレート</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1119"/>
        <source>Greatly increases accuracy by rendering decals correctly, but may cause a loss in performance</source>
        <translation>デカールを正しくレンダリングすることで精度が大幅に向上するが、パフォーマンスが低下する可能性がある</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1122"/>
        <source>Emulate N64 Depth Compare</source>
        <translation>N64の奥行きをエミュレートする</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1182"/>
        <source>Enable RetroArch Automatic Updates</source>
        <translation>RetroArchの自動アップデートを有効にする</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1192"/>
        <source>Enable Mupen64Plus-Next Automatic Updates</source>
        <translation>Mupen64Plus-Nextの自動アップデートを有効にする</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1220"/>
        <source>Use development branch</source>
        <translation>開発ブランチを使用する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1234"/>
        <source>Update Interval</source>
        <translation>アップデート間隔</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1254"/>
        <source>Every Launch</source>
        <translation>起動する度</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1259"/>
        <source>Daily</source>
        <translation>日単位</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1264"/>
        <source>Weekly</source>
        <translation>週単位</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1269"/>
        <source>Monthly</source>
        <translation>月単位</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1294"/>
        <source>Check for Updates</source>
        <translation>更新を確認</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1333"/>
        <source>Download Directory</source>
        <translation>ダウンロードフォルダ</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1345"/>
        <source>Sets the directory that hacks downloaded from romhacking.com will be saved to. If you change this setting later, your roms will automatically be moved to the new directory.</source>
        <translation>romhacking.comからダウンロードしたハックが保存されるディレクトリを設定します。後でこの設定を変更すると、ロムは自動的に新しいディレクトリに移動します。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1384"/>
        <source>Enable Star Display</source>
        <translation>スターディスプレイを有効にする</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1413"/>
        <source>If checked, Parallel Launcher will still show a star display with a default layout if the hack author did not provide one.</source>
        <translation>チェックを入れると、ハック作成者がレイアウトを提供していない場合でも、Parallel Launcherはデフォルトのレイアウトで星を表示します。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1416"/>
        <source>Show star display for hacks without a
star layout</source>
        <translation>スターレイアウトのないハックにスターを表示する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1430"/>
        <source>If checked, Parallel Launcher will submit your highest star count across all save slots to romhacking.com; otherwise, it will only submit your star count in slot A.</source>
        <translation>チェックを入れると、Parallel Launcherはすべてのセーブスロットで最も高いスター数をromhacking.comに送信します;そうでない場合は、スロットAのスター数のみを送信します。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1433"/>
        <source>Check all save slots</source>
        <translation>すべてのセーブスロットを確認</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1440"/>
        <source>If checked, Parallel Launcher will use the GLideN64 plugin instead of the ParaLLEl plugin when it guesses that ParrLLEl is probably the best plugin to use. Only check this if your computer has an old GPU with poor Vulkan support. If you still experience lag even with GlideN64, you may need to disable &apos;Emulate Framebuffer&apos; and/or &apos;Emulate N64 Depth Compare&apos; in the GFX Plugins section of your Parallel Launcher settings.</source>
        <translation>チェックを入れると、Parallel Launcherは、ParrLLElプラグインを使用するのが最適であると判断した場合、ParaLLElプラグインの代わりにGLideN64プラグインを使用します。GlideN64を使用してもラグが発生する場合は、Parallel Launcher設定のGFX Pluginsセクションで「Emulate Framebuffer」または「Emulate N64 Depth Compare」を無効にする必要があるかもしれません。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1443"/>
        <source>Prefer HLE plugins</source>
        <translation>HLEプラグインを優先</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1450"/>
        <source>If checked, Parallel Launcher will always default to using 4:3 resolution, even if the recommended settings indicate that widescreen is supported</source>
        <translation>チェックを入れると、推奨設定でワイドスクリーンがサポートされている場合でも、パラレルランチャーは常にデフォルトで4:3の解像度を使用します</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1453"/>
        <source>Ignore widescreen hint</source>
        <translation>ワイドスクリーンのヒントを無視する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1477"/>
        <source>Emulate IS Viewer hardware and display any messages sent from the ROM in a separate window</source>
        <translation>IS Viewerのハードウェアをエミュレートし、ROMから送信されたメッセージを別ウィンドウで表示する</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1480"/>
        <source>Enable IS Viewer (ParallelN64 only)</source>
        <translation>IS Viewerを有効にする(ParallelN64のみ)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1508"/>
        <source>History Size</source>
        <translation>履歴の数</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1577"/>
        <source>The following options only apply to the ParallelN64 emulator core. The mupen64plus core will always sync to the system clock.</source>
        <translation>以下のオプションは ParallelN64 エミュレータコアにのみ適用されます。mupen64plusコアは常にシステムクロックに同期します。</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1593"/>
        <source>N64 Real Time Clock</source>
        <translation>N64 リアルタイムクロック</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1600"/>
        <source>Sync to system clock</source>
        <translation>システムクロックを同期</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1613"/>
        <source>Use this time:</source>
        <translation>この時計:を使う:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1653"/>
        <source>Rollback clock when loading a savestate</source>
        <translation>セーブステートロード時にクロックをロールバックする</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="82"/>
        <source>Automatic (%1)</source>
        <translation>自動 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="331"/>
        <source>Install Discord Plugin?</source>
        <translation>ディスコードプラグインをインストールしますか？</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="331"/>
        <source>You have enabled Discord integration, but the Discord plugin is not currently installed. Would you like to install it now?</source>
        <translation>Discordの統合を有効にしましたが、Discordプラグインは現在インストールされていません。今すぐインストールしますか？</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="341"/>
        <source>Download Failed</source>
        <translation>ダウンロードに失敗しました</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="341"/>
        <source>Failed to download Discord plugin</source>
        <translation>ディスコードプラグインのダウンロードに失敗しました</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="375"/>
        <source>Auto</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="379"/>
        <location filename="../src/ui/settings-dialog.cpp" line="386"/>
        <source>Select a Folder</source>
        <translation>フォルダを選択</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="395"/>
        <source>Confirm Reset</source>
        <translation>リセットの確認</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="396"/>
        <source>This will reset your RetroArch config file, undoing any changes you have made within RetroArch. Your Parallel Launcher settings will not be affected. Do you want to continue?</source>
        <translation>これにより、RetroArchの設定ファイルがリセットされ、RetroArch内で行った変更が取り消されます。パラレルランチャーの設定は影響を受けません。続けますか？</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="403"/>
        <source>Config Reset</source>
        <translation>設定をリセット</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="403"/>
        <source>Your RetroArch config has been reset.</source>
        <translation>RetroArchの設定がリセットされました。</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="405"/>
        <source>Oops</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="405"/>
        <source>An unknown error occurred. Your RetroArch config has not been reset.</source>
        <translation>不明なエラーが発生しました。RetroArchの設定がリセットされていません。</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="424"/>
        <location filename="../src/ui/settings-dialog.cpp" line="430"/>
        <location filename="../src/ui/settings-dialog.cpp" line="436"/>
        <location filename="../src/ui/settings-dialog.cpp" line="442"/>
        <source>Confirm Apply All</source>
        <translation>全て適用の確認</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="424"/>
        <location filename="../src/ui/settings-dialog.cpp" line="430"/>
        <location filename="../src/ui/settings-dialog.cpp" line="436"/>
        <location filename="../src/ui/settings-dialog.cpp" line="442"/>
        <source>Apply this setting to all current roms?</source>
        <translation>この設定を現在のすべてのロムに適用しますか？</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="454"/>
        <source>Unlimited</source>
        <translation>無制限</translation>
    </message>
</context>
<context>
    <name>SingleplayerControllerSelectDialog</name>
    <message>
        <location filename="../src/ui/designer/singleplayer-controller-select-dialog.ui" line="20"/>
        <source>Controller Select</source>
        <translation>コントローラーを選択</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/singleplayer-controller-select-dialog.ui" line="32"/>
        <source>Multiple controllers are connected. Please press any button on the controller you wish to use, or press Esc on the keyboard to use keyboard controls.</source>
        <translation>複数のコントローラーが接続されています。使用したいコントローラーの任意のボタンを押すか、キーボードのEscを押してキーボード操作を行ってください。</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../src/updaters/update-dialog.ui" line="14"/>
        <source>Update Available</source>
        <translation>アップデートがあります</translation>
    </message>
    <message>
        <location filename="../src/updaters/update-dialog.ui" line="26"/>
        <source>A new update is available for Parallel Launcher. Would you like to update now?</source>
        <translation>Parallel Launcherに新しいアップデートがあります。今すぐアップデートしますか？</translation>
    </message>
    <message>
        <location filename="../src/updaters/update-dialog.ui" line="62"/>
        <source>Changelog:</source>
        <translation>更新履歴:</translation>
    </message>
    <message>
        <location filename="../src/updaters/update-dialog.ui" line="79"/>
        <source>Don&apos;t remind me again</source>
        <translation>今後表示しない</translation>
    </message>
</context>
<context>
    <name>WindowsRetroArchUpdater</name>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="112"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="33"/>
        <source>Checking for RetroArch Updates</source>
        <translation>RetroArchのアップデートを確認</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="221"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="120"/>
        <source>Download Failed</source>
        <translation>ダウンロードに失敗しました</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="222"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="121"/>
        <source>Failed to download RetroArch.</source>
        <translation>RetroArchのダウンロードに失敗しました。</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="229"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="128"/>
        <source>Installing RetroArch</source>
        <translation>RetroArchをインストールしています</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="245"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="144"/>
        <source>Installation Error</source>
        <translation>インストールのエラー</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="246"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="145"/>
        <source>An error occurred attempting to uncompress the portable RetroArch bundle</source>
        <translation>ポータブルRetroArchの紐づけを解除しようとするとエラーが発生しました</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="287"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="174"/>
        <source>Install Update?</source>
        <translation>アップデートをインストールしますか？</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="288"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="175"/>
        <source>An update for RetroArch is available. Would you like to install it now?</source>
        <translation>RetroArchのアップデートが利用可能です。今すぐインストールしますか？</translation>
    </message>
</context>
<context>
    <name>WindowsUpdater</name>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="45"/>
        <source>Unexpected Error</source>
        <translation>予期せぬエラー</translation>
    </message>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="46"/>
        <source>Failed to launch installer.</source>
        <translation>インストーラーの起動に失敗しました。</translation>
    </message>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="57"/>
        <source>Download Failed</source>
        <translation>ダウンロードに失敗しました</translation>
    </message>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="58"/>
        <source>Failed to download the latest installer. Try again later.</source>
        <translation>最新のインストーラのダウンロードに失敗しました。後で再試行してください。</translation>
    </message>
</context>
</TS>
